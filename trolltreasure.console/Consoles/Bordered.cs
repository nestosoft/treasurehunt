﻿using Microsoft.Xna.Framework;
using SadConsole;

namespace trolltreasure.console.Consoles
{
    public class Bordered : Console
    {
        private readonly SadConsole.Surfaces.Basic _borderSurface;

        public Bordered(int width, int height, string title) : base(width, height)
        {
            Cursor.Position = new Point(0, 0);

            const int offset = 2;
            _borderSurface = new SadConsole.Surfaces.Basic(width + offset, height + offset, base.Font);
            _borderSurface.DrawBox(new Rectangle(0, 0, _borderSurface.Width, _borderSurface.Height), 
                new Cell(Color.White, Color.Black), null, SadConsole.Surfaces.SurfaceBase.ConnectedLineThin);
            _borderSurface.Print(2,0, $":{title}:");
            _borderSurface.Position = new Point(-1, -1);

            Children.Add(_borderSurface);
        }        
    }
}