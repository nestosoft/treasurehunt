﻿using Microsoft.Xna.Framework.Input;
using SadConsole.Renderers;
using trolltreasure.game.Domain;
using trolltreasure.game.Services;
using Window = SadConsole.Window;

namespace trolltreasure.console.Consoles
{
    public class Character : Window
    {
        public Character(int width, int height, GameObject obj) : base(width, height)
        {
            Title = obj.Name;
            Print(2, 3, "Stats ----------------------");
            Print(2, 5, $"- HP : {obj.Fighter.HP}");
            Print(2, 6, $"- MaxHP : {obj.Fighter.MaxHP}");
            Print(2, 7, $"- Strength : {obj.Fighter.Power}");
            Print(2, 8, $"- Agility : {obj.Fighter.Defense}");
            Print(2, 9, $"- Level : {obj.Fighter.Level}");
            Print(2, 10, $"- Xp : {obj.Fighter.Xp}");
            Print(2, 11, $"- To next level : {LevelUpService.ToLevelUp(obj.Fighter)}");
            Print(2, 15, "EXIT (Q or ESC)");

        }

        public override bool ProcessKeyboard(SadConsole.Input.Keyboard info)
        {
            if (info.IsKeyReleased(Keys.Escape) || info.IsKeyReleased(Keys.Q))
            {
                Hide();
            }

            return true;
        }
    }
}