﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SadConsole;
using trolltreasure.console.Events;
using trolltreasure.console.Services;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Services;
using trolltreasure.game.Services.Factories;
using Keyboard = SadConsole.Input.Keyboard;

namespace trolltreasure.console.Consoles
{
    public class GamePlay : SadConsole.Console
    {
        public event EventHandler StopGamePlay;
        
        private readonly MoveService _moveService = new MoveService();
        private readonly TurnBasedService _turnBasedService = new TurnBasedService();
        private readonly InventoryService _inventoryService = new InventoryService();
        private readonly LevelUpService _levelUpService = new LevelUpService();
        private readonly List<Log> _logs = new List<Log>();
        private Dungeon _dungeon;
        private bool _takeTurn = true;
        private bool _isGameOver;
        // consoles
        private readonly LogConsole _logsConsole;
        private readonly Bordered _hud;
        private readonly Bordered _optionMenu;
        private MapConsole _map;

        public GamePlay(int width, int height): base(width, height)
        {          
            _dungeon = new Dungeon(1); //start in level 1
            // get initial position
            var startPosition = _dungeon.EntryPosition();
            // Create Player
            var player = MonsterFactory.Player(startPosition, "Player", _dungeon.Map.Tiles); 
            // Place player in the dungeon
            _dungeon.PlacePlayer(player);
            
            // setting up other consoles
            var wideWidth = (width * 6 / 10) - 2 ;
            var narrowWidth = width - wideWidth - 4;
            var shortHeight = (height * 2 / 10) - 1;
            var longHeight = height - shortHeight -4;
            
            _logsConsole = new LogConsole(wideWidth, shortHeight, "Logs"); //100 lines of logs for now
            _logsConsole.Position = new Point(1,1);
            _logs.Add(new Log("Welcome stranger! Prepare to perish looking for the Troll's treasure", MessageType.Introduction));
            Children.Add(_logsConsole);
            
            _hud = new Bordered(narrowWidth, longHeight, "HUD");
            _hud.Position = new Point(wideWidth + 3, shortHeight + 3);
            Children.Add(_hud);
            
            _optionMenu = new Bordered(narrowWidth, shortHeight, "Options");
            _optionMenu.Position = new Point(wideWidth + 3 , 1);
            Children.Add(_optionMenu);
            
            //_game = new SadConsole.Console(wideWidth, longHeight);
            _map = new MapConsole(_dungeon.Map.Tiles.GetLength(0), _dungeon.Map.Tiles.GetLength(1));
            _map.Position = new Point(1, shortHeight +3 );
            _map.ViewPort = new Rectangle(0,0, wideWidth, longHeight);
            Children.Add(_map);
        }

        private void ScrollPage(int dx, int dy)
        {
            var x = _map.ViewPort.X + dx;
            var y = _map.ViewPort.Y + dy;
            _map.ViewPort = new Rectangle(x, y, _map.ViewPort.Width, _map.ViewPort.Height);
        }



        private void PrintObjectName()
        {
            // show mouse position for the map
            var point = _map.MousePosition;
            var pos = new Position(point.X, point.Y);
            var mouseObjects = _dungeon.Objects.FindAll(o => o.Position.Equals(pos) && _dungeon.Player.Fov.IsPositionVisible(o.Position.X, o.Position.Y));
            _optionMenu.Clear();
            _optionMenu.Cursor.Position = new Point(1, 1);
            _optionMenu.Cursor.Print($"Mouse position: ({point.X},{point.Y})\n");
            var newPos = new Point(1, _optionMenu.Cursor.Position.Y + 1);
            foreach (var o in mouseObjects)
            {
                _optionMenu.Cursor.Position = newPos;
                _optionMenu.Cursor.Print($"{o.Name} position: ({o.Position.X},{o.Position.Y})\n");
                newPos = new Point(1, newPos.Y + 1);
            }
            try
            {
                Tile tile = _dungeon.Map.Tiles[point.X, point.Y];
                _optionMenu.Cursor.Print($"tile ({point.X},{point.Y}) -> blocked? {tile.Blocked}, blocksight? {tile.BlockSight}");
            }
            catch (IndexOutOfRangeException)
            {
                _optionMenu.Cursor.Print($"tile ({point.X},{point.Y}) - out of the range");
            }
        }

        public override void Update(TimeSpan delta)
        {
            // only reprint map when player has moved
            // and player is alive
            if (_takeTurn && !_isGameOver)
            {
                // reset flag
                _takeTurn = false;
                // center viewport on the player
                _map.CenterPosition(_dungeon.Player.Position.X, _dungeon.Player.Position.Y);
                // print visible cells and visited cells
                MapDrawingService.Draw(_map,
                    _dungeon.Map,
                    _dungeon.Player.Visible,
                    _dungeon.Player.Visited);

                // Take a turn for all the objects
                var result = _turnBasedService.TakeTurn(_dungeon);
                // aggregateResults to logs
                _logs.AddRange(result.Logs);
                
                // reset hud cursor position
                _hud.Clear();
                _hud.Cursor.Position = new Point(0, 1);
                // print visible objects and stats
                foreach (var o in result.ObjectsToDraw)
                {
                    ObjectDrawingService.Draw(_map, o);
                    HudDrawingService.PrintObjectStats(_hud, o);
                }

                if (_dungeon.Player.Fighter.CheckStatus() == FighterStatus.Dead)
                {
                    _isGameOver = true;
                    Window.Message("GAME OVER!", "Close", () => StopGamePlay?.Invoke(this, new EventArgs()));
                }
                if (_levelUpService.LevelUp(_dungeon.Player.Fighter))
                {
                    var level = new LevelUp(30, 10, _dungeon.Player);
                    level.Success = LevelUp;
                    level.Center();
                    level.Show(true); 
                }
                
            }
            // Print the last 8 logs
            _logsConsole.PrintLog(_logs);

            // print name of object selected with the mouse
            PrintObjectName();
            base.Update(delta);
        }
        

        public override bool ProcessKeyboard(Keyboard info)
        {
            if(info.IsKeyReleased(Keys.P) || info.IsKeyReleased(Keys.Q))
            {
                StopGamePlay?.Invoke(this, new EventArgs());
            }else if (info.IsKeyReleased(Keys.Up))
            {
                var result = _moveService.MoveOrAttack(_dungeon.Player, 0, -1, _dungeon.Map.Tiles, new List<GameObject>(_dungeon.Objects));
                _logs.AddRange(result.Logs);
                _takeTurn = true;
            }else if (info.IsKeyReleased(Keys.Down))
            {
                var result = _moveService.MoveOrAttack(_dungeon.Player, 0, +1, _dungeon.Map.Tiles, new List<GameObject>(_dungeon.Objects));
                _logs.AddRange(result.Logs);
                _takeTurn = true;
            }else if (info.IsKeyReleased(Keys.Left))
            {
                var result = _moveService.MoveOrAttack(_dungeon.Player, -1, 0, _dungeon.Map.Tiles, new List<GameObject>(_dungeon.Objects));
                _logs.AddRange(result.Logs);
                _takeTurn = true;
            }else if (info.IsKeyReleased(Keys.Right))
            {
                var result = _moveService.MoveOrAttack(_dungeon.Player, +1, 0, _dungeon.Map.Tiles, new List<GameObject>(_dungeon.Objects));
                _logs.AddRange(result.Logs);
                _takeTurn = true;
            }else if (info.IsKeyReleased(Keys.Delete))
            {
                _map.CenterPosition(_dungeon.Player.Position.X, _dungeon.Player.Position.Y);
            }else if (info.IsKeyDown(Keys.PageUp))
            {
                ScrollPage(0, -30);
            }else if (info.IsKeyDown(Keys.PageDown))
            {
                ScrollPage(0, +30);
            }else if (info.IsKeyDown(Keys.Home))
            {
                ScrollPage(-30, 0);
            }else if (info.IsKeyDown(Keys.End))
            {
                ScrollPage(+30, 0);
            }else if (info.IsKeyReleased(Keys.G))
            {
                var result = _inventoryService.PickUp(_dungeon.Player, _dungeon);
                _logs.AddRange(result.Logs);
                _takeTurn = true;
            }else if (info.IsKeyReleased(Keys.U))
            {
                var inv = new Inventory(50, 20, _dungeon.Player.Items, "Use Item");
                inv.SelectedCallBack = InventoryUseCallBack;
                inv.Center();
                inv.Show(true);
            }else if (info.IsKeyReleased(Keys.D))
            {
                var inv = new Inventory(50, 20, _dungeon.Player.Items, "Drop Item");
                inv.SelectedCallBack = InventoryDropCallBack;
                inv.Center();
                inv.Show(true);
                
            }else if (info.IsKeyReleased(Keys.C))
            {
                var character = new Character(50, 30, _dungeon.Player);
                character.Center();
                character.Show(true);
            }
            
            else if (info.IsKeyReleased(Keys.S))
            {
                // enter select mode
                EnterSelectMode(null);
            }else if (info.IsKeyReleased(Keys.OemComma) || (info.IsKeyDown(Keys.LeftShift) && info.IsKeyDown(Keys.OemComma)))
            {
                // next level 
                NextLevel();
            }

            return true; 
        }

        private void NextLevel()
        {
            if (_dungeon.Player.Position.Equals(_dungeon.ExitPosition()))
            {
                var oldLevel = _dungeon;
                // new Dungeon
                _dungeon = new Dungeon(_dungeon.Level+1);
                // Place player
                var newplayer = MonsterFactory.Player(_dungeon.EntryPosition(), oldLevel.Player.Name, _dungeon.Map.Tiles);
                // same fighter skills and Items
                newplayer.Fighter = oldLevel.Player.Fighter;
                newplayer.Items = oldLevel.Player.Items;
                // recover a little bit
                _logs.Add(new Log("You take a moment to rest, and recover your strength.", MessageType.Introduction));
                newplayer.Fighter.Heal(newplayer.Fighter.MaxHP/2);
                _logs.Add(new Log("After a rare moment of peace, you descend deeper into the heart of the dungeon...", MessageType.Introduction));
                _dungeon.PlacePlayer(newplayer);
                // reset map
                ResetMap();
                // take a turn to redraw
                _takeTurn = true;
            }
            else
            {
                _logs.Add(new Log("There is no stairs in this position", MessageType.ActionFailed));
            }
        }

        private void ResetMap()
        {
            Children.Remove(_map);
            var oldMapConsole = _map;
            _map = new MapConsole(_dungeon.Map.Tiles.GetLength(0), _dungeon.Map.Tiles.GetLength(1));
            _map.Position = oldMapConsole.Position;
            _map.ViewPort = oldMapConsole.ViewPort;
            Children.Add(_map);
        }

        private void EnterSelectMode(Item resultSelectedItem)
        {
            var centerPlayer = new Point(_dungeon.Player.Position.X, _dungeon.Player.Position.Y);
            _map.SelectedPosition = centerPlayer;
            _map.Item = resultSelectedItem;
            _map.backToGame = BackToGame;
            _map.selected = SelectedPosition;
            _map.SelectMode = true;
            Global.FocusedConsoles.Set(_map);
        }

        private void LevelUp(object sender, EventArgs args)
        {
            if (args is ResultEventArgs result && result.Result != null && result.Result.Logs != null)
            {
                _logs.AddRange(result.Result.Logs);
            }
        }

        private void BackToGame(object sender, EventArgs args)
        {
            Global.FocusedConsoles.Set(this);
        }
        
        private void SelectedPosition(object sender, EventArgs e)
        {
            var result = e as SelectEventArgs;
            if (result?.Selected != null && result.Item != null)
            {
                var usableResult = _inventoryService.UseItem(_dungeon.Player, result.Item, _dungeon, result.Selected);
                _logs.AddRange(usableResult.Logs);
                _takeTurn = true;
                Global.FocusedConsoles.Set(this);
            }

        }
        
        private void InventoryUseCallBack(object sender, EventArgs args)
        {
            if (!(args is InventoryEventArgs result) || result.SelectedItem == null) return;
            var isUsable = result.SelectedItem.Use != null;

            if (isUsable && result.SelectedItem.Use.Targetable)
            {
                EnterSelectMode(result.SelectedItem);
            }
            else
            {
                var usableResult = _inventoryService.UseItem(_dungeon.Player, result.SelectedItem, _dungeon, null);
                _logs.AddRange(usableResult.Logs);
                _takeTurn = true;
            } 
        }

        private void InventoryDropCallBack(object sender, EventArgs args)
        {
            if (!(args is InventoryEventArgs result) || result.SelectedItem == null) return;
            
            var dropResult = _inventoryService.DropItem(_dungeon.Player, result.SelectedItem, _dungeon);
            _logs.AddRange(dropResult.Logs);
            _takeTurn = true;
        }

    }
}