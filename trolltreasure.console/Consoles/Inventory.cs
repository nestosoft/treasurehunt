﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SadConsole;
using SadConsole.Controls;
using trolltreasure.console.Events;
using trolltreasure.game.Domain;

namespace trolltreasure.console.Consoles
{
    public class Inventory : Window
    {
        
        private readonly List<Item> _items;
        private Item _itemSelected;
        public EventHandler SelectedCallBack;
        
        public Inventory(int width, int height, List<Item> items, string action) : base(width, height)
        {
            Title = $"Inventory to {action}";
            var closeButton = new Button(11, 1);
            closeButton.Position = new Point(width-15, height-3);
            closeButton.Text = "Close (Q)";
            closeButton.Click += (btn, args) => Hide();
            Add(closeButton);
            var useButton = new Button(18, 1);
            useButton.Position = new Point(1, height-3);
            useButton.Text = "Select (S)";
            useButton.Click += (btn, args) => SelectItem();
            Add(useButton);

            _items = items;
            ShowItems();
        }

        

        public override bool ProcessKeyboard(SadConsole.Input.Keyboard info)
        {
            if (info.IsKeyReleased(Keys.Escape) || info.IsKeyReleased(Keys.Q))
            {
                Hide();
            }else if (info.IsKeyReleased(Keys.S))
            {
                SelectItem();
            }
            
            var keys = info.KeysReleased;
            if (keys.Any(k => (k.Key >= Keys.D0 && k.Key <= Keys.D9) || (k.Key >= Keys.NumPad0 && k.Key <= Keys.NumPad9)))
            {
                var indexSelected = int.Parse(keys.First().Character.ToString()); //get the number
                try
                {
                    _itemSelected = _items[indexSelected];
                    SelectItem();
                }
                catch (ArgumentOutOfRangeException)
                {
                    // do nothing
                }
                
            }

            return base.ProcessKeyboard(info);
        }

        private void SelectItem()
        {
            Hide();
            var arg = new InventoryEventArgs(_itemSelected);
            SelectedCallBack?.Invoke(this, arg);
        }

        private void ShowItems()
        {
            var pos = new Point(1,1);
            for (var i = 0; i < _items.Count; i++ )
            {
                pos = new Point(pos.X, pos.Y + 1);
                var text = $"{_items[i].Name} ({i})";
                if (_items[i].Equipment != null && _items[i].Equipment.IsEquipped)
                    text += $" (Equipped on {_items[i].Equipment.Slot})";
                var radio = new RadioButton(Width - 2, 1) {Text = text};
                var i1 = i;
                radio.IsSelectedChanged += (r, args) => _itemSelected = _items[i1];
                radio.Position = pos;
                Add(radio);
                if (i == 0) FocusedControl = radio;
            }
        } 
    }
}