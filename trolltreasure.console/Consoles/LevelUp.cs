﻿using System;
using Microsoft.Xna.Framework;
using SadConsole;
using SadConsole.Controls;
using trolltreasure.console.Events;
using trolltreasure.game.Domain;
using trolltreasure.game.Services;

namespace trolltreasure.console.Consoles
{
    public class LevelUp : Window
    {
        private readonly LevelUpService _service = new LevelUpService();
        public EventHandler Success; 
        
        public LevelUp(int width, int height, GameObject obj) : base(width, height)
        {
            Title = $"Level Up {obj.Name}";
            var button = new Button(width-2, 1);
            button.Position = new Point(1, 1);
            button.Text = "Constitution (+20 HP) (0)";
            button.Click += (btn, args) =>
            {
                var result =_service.LevelUpConstitution(obj.Fighter);
                Success?.Invoke(this, new ResultEventArgs(result));
                Hide();
            };
            Add(button);
            button = new Button(width-2, 1);
            button.Position = new Point(1, 2);
            button.Text = "Strength (+1 attack) (1)";
            button.Click += (btn, args) =>
            {
                var result = _service.LevelUpStrength(obj.Fighter);
                Success?.Invoke(this, new ResultEventArgs(result));
                Hide();
            };
            Add(button);
            button = new Button(width-2, 1);
            button.Position = new Point(1, 3);
            button.Text = "Agility (+1 defense)";
            button.Click += (btn, args) =>
            {
                var result = _service.LevelUpAgility(obj.Fighter);
                Success?.Invoke(this, new ResultEventArgs(result));
                Hide();
            };
            Add(button);
        }
    }
}