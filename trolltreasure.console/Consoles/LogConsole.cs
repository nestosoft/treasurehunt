﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using SadConsole;
using SadConsole.Surfaces;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using Console = SadConsole.Console;

namespace trolltreasure.console.Consoles
{
    public class LogConsole : Console
    {
        public LogConsole(int width, int height, string title) : base(width, height)
        {
            const int offset = 2;
            var borderSurface = new Basic(width + offset, height + offset, Font);
            borderSurface.DrawBox(new Rectangle(0, 0, borderSurface.Width, borderSurface.Height), 
                new Cell(Color.White, Color.Black), null, ConnectedLineThin);
            borderSurface.Print(2,0, $":{title}:");
            borderSurface.Position = new Point(-1, -1);
            
            Children.Add(borderSurface);
        }

        public void PrintLog(List<Log> logs)
        {
            if (logs != null)
            {
                Clear();
                
                var consoleLines = Height-1;
                var filtered = logs.FindAll(l => !l.Type.Equals(MessageType.Movement));
                var lastEight  = filtered.Skip(Math.Max(0, filtered.Count - consoleLines)).ToList();
                lastEight.Reverse(); // in revers order
                foreach (var log in lastEight)
                {
                    if (consoleLines < 0) break;
                    
                    var doubleLine = log.Message.Length > Width;
                    if (doubleLine)
                    {
                        consoleLines -= 1;
                    }

                    TextColor(log, out var bg, out var fg);
                    var text = new ColoredString(log.Message, fg, bg);
                    Print(0, consoleLines, text);
                    consoleLines -= 1;
                }
            }
        }

        private static void TextColor(Log log, out Color bg, out Color fg)
        {
            fg = Color.White;
            bg = Color.Black;
            switch (log.Type)
            {
                case MessageType.Cast:
                    fg = Color.DarkSeaGreen;
                    break;
                case MessageType.Introduction:
                    fg = Color.Aqua;
                    break;
                case MessageType.ActionFailed:
                    fg = Color.Red;
                    break;
                case MessageType.ActionSuccess:
                    fg = Color.Green;
                    break;
                case MessageType.OnFire:
                    fg = Color.Orange;
                    break;
            }
        }
    }
}