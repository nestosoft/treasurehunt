﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SadConsole;
using SadConsole.Controls;
using trolltreasure.console.Settings;

namespace trolltreasure.console.Consoles
{
    public class MainMenu : ControlsConsole
    {

        public event EventHandler QuitGame;
        public event EventHandler StartGame;
        public event EventHandler ContinueGame;
        public event EventHandler Test;
        
        public MainMenu(int width, int height) : base(width, height)
        {
            // TODO: add background image
            
            Fill(new Rectangle(0, 0, width, height), null, ThemeColor.TitleBg, 0, SpriteEffects.None);
            var middleWidth = width / 4;
            var middleHeight = height / 4;
            Print(middleWidth, middleHeight, "Troll's Treasure", ThemeColor.TitleFg);

           
            var playButton = new SelectionButton(middleWidth, 1);
            playButton.Text = "Play a new game (P)";
            playButton.Position = new Point(middleWidth, middleHeight + 2 );
            playButton.Click += (s, a) => StartGame?.Invoke(this, new EventArgs());
            Add(playButton);
            
 
            var continueButton = new SelectionButton(middleWidth, 1);
            continueButton.Text = "Continue last game (C)";
            continueButton.Position = new Point(middleWidth, middleHeight + 4 );
            //continueButton.Click += (s, a) => OtherMenuGame?.Invoke(this, new EventArgs()); 
            continueButton.Click += (s, a) => ContinueGame?.Invoke(this, new EventArgs()); 

            //continueButton.Click += (s, a) => Window.Message("Load game has been clicked!", "Close");
            Add(continueButton);
            
            var quitButton = new SelectionButton(middleWidth, 1);
            quitButton.Text = "Quit (Q)";
            quitButton.Position = new Point(middleWidth, middleHeight + 6 );
            quitButton.Click += (s, a) => QuitGame?.Invoke(this, new EventArgs());
            Add(quitButton);
            
            playButton.PreviousSelection = quitButton;
            playButton.NextSelection = continueButton;
            continueButton.PreviousSelection = playButton;
            continueButton.NextSelection = quitButton;
            quitButton.PreviousSelection = continueButton;
            quitButton.NextSelection = playButton;


            var testButton = new Button(15, 1);
            testButton.Text = "Test Consoles";
            testButton.Position = new Point(0, 0);
            testButton.Click += (s, a) => Test?.Invoke(this, new EventArgs());
            Add(testButton);
        }

        public override bool ProcessKeyboard(SadConsole.Input.Keyboard info)
        {
            if(info.IsKeyReleased(Keys.P))
            {
                StartGame?.Invoke(this, new EventArgs());
            }else if (info.IsKeyReleased(Keys.C))
            {
                ContinueGame?.Invoke(this, new EventArgs());    
            }else if (info.IsKeyReleased(Keys.Q))
            {
                QuitGame?.Invoke(this, new EventArgs());
            }
            
            return base.ProcessKeyboard(info);
        }
        
    }
}