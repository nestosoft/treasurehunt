﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SadConsole.Input;
using SadConsole.Surfaces;
using trolltreasure.console.Events;
using trolltreasure.game.Domain;
using Keyboard = SadConsole.Input.Keyboard;

namespace trolltreasure.console.Consoles
{
    public class MapConsole : SadConsole.Console
    {
        public Point SelectedPosition;
        public Point MousePosition;
        private readonly Basic _select;
        private readonly Basic _debug;
        public EventHandler backToGame;
        public EventHandler selected;
        public bool SelectMode;
        public Item Item;

        public MapConsole(int width, int height) : base(width, height)
        {
            _select = new Basic(1, 1);
            Children.Add(_select);
            
            _debug = new Basic(50,10);
            _debug.Position = new Point(0, 0);
            Children.Add(_debug);
        }
        
        public override bool ProcessMouse(MouseConsoleState state)
        {
            MousePosition = SelectMode ? state.ConsolePosition : state.CellPosition;

            if (state.Mouse.RightClicked)
            {
                SelectedPosition = MousePosition;
            }

            return base.ProcessMouse(state);
        }

        public override void Update(TimeSpan delta)
        {
            if (SelectMode)
            {
                _select.IsVisible = true;
                PrintArrow();
                // PrintDebugPosition();
            }
            
            // base.Update(delta);
        }

        public override bool ProcessKeyboard(Keyboard info)
        {
            if (info.IsKeyReleased(Keys.Up))
            {
                SelectedPosition.Y -= 1;
            }else if (info.IsKeyReleased(Keys.Down))
            {
                SelectedPosition.Y += 1;
            }else if (info.IsKeyReleased(Keys.Left))
            {
                SelectedPosition.X -= 1;
            }else if (info.IsKeyReleased(Keys.Right))
            {
                SelectedPosition.X += 1;
            }else if (info.IsKeyReleased(Keys.Escape) || info.IsKeyReleased(Keys.Q))
            {
                _select.IsVisible = false;
                SelectMode = false;
                backToGame?.Invoke(this, new EventArgs());
                // back to gamePlay mode
            }else if (info.IsKeyReleased(Keys.Enter))
            {
                // select tile
                var selectedEvent = new SelectEventArgs(new Position(SelectedPosition.X, SelectedPosition.Y), Item);
                _select.IsVisible = false;
                SelectMode = false;
                selected?.Invoke(this, selectedEvent);
            }

            return true; // base.ProcessKeyboard(info);
        }

        private void PrintArrow()
        {
            _select.Position = new Point(SelectedPosition.X-ViewPort.X, SelectedPosition.Y- ViewPort.Y);
            _select.Print(0, 0, "X", Color.Red, Color.Beige);
        }

        private void PrintDebugPosition()
        {   
            _debug.Print(0,0, $"targeting -> {SelectedPosition}");
            _debug.Print(0,1, $"MousePosition -> {MousePosition}");
            _debug.Print(0,2, $"SelectedPosition -> {SelectedPosition}");
            _debug.Print(0,3, $"ViewPort -> {ViewPort}");
            _debug.Print(0,4, $"Mark postion -> {_select.Position}");
        }
        
        public void CenterPosition(int x, int y)
        {
            var h = y - ViewPort.Height / 2 ;
            var m = x - ViewPort.Width / 2;
            h = h < 0 ? 0 : h;
            m = m < 0 ? 0 : m;
            ViewPort = new Rectangle(m, h, ViewPort.Width, ViewPort.Height);
        }
    }
}