﻿using System;
using trolltreasure.game.Domain;

namespace trolltreasure.console.Events
{
    public class InventoryEventArgs : EventArgs
    {
        public readonly Item SelectedItem;

        public InventoryEventArgs(Item selected)
        {
            SelectedItem = selected;
        }
    }
}