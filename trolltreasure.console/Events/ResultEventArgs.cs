﻿using System;
using System.Collections.Generic;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.console.Events
{
    public class ResultEventArgs : EventArgs
    {
        public Result Result;

        public ResultEventArgs(Result result)
        {
            Result = result;
        }
    }
}