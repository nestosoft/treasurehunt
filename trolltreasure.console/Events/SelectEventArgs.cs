﻿using System;
using System.Security.Cryptography.X509Certificates;
using trolltreasure.game.Domain;

namespace trolltreasure.console.Events
{
    public class SelectEventArgs : EventArgs
    {
        public Position Selected { get; }
        public Item Item { get; }

        public SelectEventArgs(Position pos, Item item)
        {
            Selected = pos;
            Item = item;
        }
    }
}