﻿using Microsoft.Xna.Framework;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;

namespace trolltreasure.console.Services
{
    public static class HudDrawingService
    {
        public static void PrintObjectStats(SadConsole.Console console, GameObject o)
        {
            var positionCursor = console.Cursor.Position;
            var hp = o.Fighter?.HP ?? 0;
            var isDead = o.Fighter?.Status == FighterStatus.Dead;
            var name = isDead ? $"Remaining of {o.Name}" : o.Name;
            console.Cursor.Print($"{name}:HP->{hp}, Pos->({o.Position.X},{o.Position.Y})\n");
            console.Cursor.Position = new Point(0, positionCursor.Y + 1);
        }
    }
}