﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using SadConsole;
using trolltreasure.console.Settings;
using trolltreasure.game.Domain;

namespace trolltreasure.console.Services
{
    public static class MapDrawingService
    {
        public static void Draw(Console console, Map map, List<Point> visibleCells, List<Point> visitedCells)
        {
            // To avoid render the same points twice
            var vistedNotInView = visitedCells.Except(visibleCells);
            
            foreach (var cell in vistedNotInView)
            {
                var wall = map.Tiles[cell.X, cell.Y].Blocked;
                if (wall)
                {
                    console.Print(cell.X, cell.Y, "#", ThemeColor.Wall);
                }
                else
                {
                    console.Print(cell.X, cell.Y, " ", ThemeColor.Ground);
                }
            }
            
            foreach (var cell in visibleCells)
            {
                var wall = map.Tiles[cell.X, cell.Y].Blocked;
                if (wall)
                {
                    console.Print(cell.X, cell.Y, "#", ThemeColor.VisibleWall);
                }
                else
                {
                    console.Print(cell.X, cell.Y, ".", ThemeColor.VisibleGround);
                }
                visitedCells.Add(new Point(cell.X, cell.Y));
            }
        }
    }
}