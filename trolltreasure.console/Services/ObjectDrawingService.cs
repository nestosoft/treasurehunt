﻿using Microsoft.Xna.Framework;
using SadConsole;
using trolltreasure.console.Settings;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;

namespace trolltreasure.console.Services
{
    public static class ObjectDrawingService
    {
        public static void Draw(Console console, GameObject o)
        {
            var isDead = o.Fighter?.Status == FighterStatus.Dead;
            var symbol = isDead ? "%" : Symbol(o);
            var color = isDead ? ThemeColor.DeadBody : ObjectRepresentationService.ColorMapping[o.ObjectType];
            console.Print(o.Position.X, o.Position.Y, symbol, color);
        }

        private static string Symbol(GameObject o)
        {
            return o.Item != null
                ? ObjectRepresentationService.SymbolItemMapping[o.Item.Type]
                : ObjectRepresentationService.SymbolMapping[o.ObjectType];
        }
    }
}