﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using trolltreasure.console.Settings;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;

namespace trolltreasure.console.Services
{
    public static class ObjectRepresentationService
    {
        public static readonly Dictionary<ObjectType, string> SymbolMapping = new Dictionary<ObjectType, string>()
        {
            {ObjectType.Human, "@"},
            {ObjectType.Troll, "T"},
            {ObjectType.Orc, "o"},
            {ObjectType.Item, "i"},
            {ObjectType.StairsDown, "<"},
            {ObjectType.StairsUp, ">"},

        };
        
        public static readonly Dictionary<ObjectType, Color> ColorMapping = new Dictionary<ObjectType, Color>()
        {
            {ObjectType.Human, ThemeColor.Player},
            {ObjectType.Troll, ThemeColor.Troll},
            {ObjectType.Orc, ThemeColor.Orc},
            {ObjectType.Item, ThemeColor.Potion},
            {ObjectType.StairsDown, ThemeColor.Stairs},
            {ObjectType.StairsUp, ThemeColor.Stairs},

        };

        public static readonly Dictionary<ItemType, string> SymbolItemMapping = new Dictionary<ItemType, string>()
        {
            {ItemType.Potion, "!"},
            {ItemType.Scroll, "&"},
            {ItemType.Sword, "/"},
            {ItemType.Shield, "O"}
        };

    }
}