﻿namespace trolltreasure.console.Settings
{
    public enum Resolution
    {
        OldSchoolConsole_C64_16x16,
        OldSchoolConsole_Cheepicus_12x12,
        OldSchoolConsole_IBM_8x16,
        OldSchoolConsole_IBM_8x16_EXT,
        Phone,
        ModernPhone,
        Tablet,
        HD,
        FHD,
        Inherit_16X16
    }
}