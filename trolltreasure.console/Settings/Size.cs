﻿namespace trolltreasure.console.Settings
{
    public class Size
    {
        public int Width;
        public int Height;
        public string Font;

        public Size(int width, int height, string fontLocation)
        {
            Width = width;
            Height = height;
            Font = fontLocation;
        }
    }
}