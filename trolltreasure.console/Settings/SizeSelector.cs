﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace trolltreasure.console.Settings
{
    public static class SizeSelector
    {
        public static readonly Dictionary<Resolution, Size> Resolutions = new Dictionary<Resolution, Size>()
        {
            {Resolution.OldSchoolConsole_C64_16x16, new Size(80,25,"fonts/C64.font")},
            {Resolution.OldSchoolConsole_Cheepicus_12x12, new Size(80,25,"fonts/Cheepicus12.font")},
            {Resolution.OldSchoolConsole_IBM_8x16, new Size(80,25,"fonts/IBM.font")},
            {Resolution.OldSchoolConsole_IBM_8x16_EXT, new Size(80,25,"fonts/IBM_ext.font")},
            {Resolution.HD, new Size(1280/16,720/16,"fonts/C64.font")}, //1280x720
            {Resolution.FHD, new Size(1920/16,1080/16,"fonts/C64.font")}, //1920x1080
        };
    }
}