﻿using Microsoft.Xna.Framework;

namespace trolltreasure.console.Settings
{
    public static class ThemeColor
    {
        // Menu
        public static Color TitleBg = Color.Black;
        public static Color TitleFg = ColorAnsi.YellowBright;
        
        // Map
        public static Color Player = ColorAnsi.CyanBright;
        public static Color Npc = ColorAnsi.YellowBright;
        public static Color Wall = Color.Gray;
        public static Color VisibleWall = Color.Orange;
        public static Color Ground = Color.DarkGray;
        public static Color VisibleGround = Color.Yellow;
        public static Color Orc = Color.DarkGreen;
        public static Color Troll = Color.Green;

        public static Color DeadBody = Color.DarkRed;
        public static Color Potion = Color.Violet;
        public static Color Stairs = Color.SandyBrown;
    }
}