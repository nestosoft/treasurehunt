using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SadConsole;
using trolltreasure.console.Consoles;
using trolltreasure.console.Settings;
using Game = SadConsole.Game;

namespace trolltreasure.console
{
    
    internal static class Program
    {
        
        private static MainMenu _mainMenu;
        private static GamePlay _game;
        private static readonly Size Size = SizeSelector.Resolutions[Resolution.HD];

        private static void Main()
        {
            
            // Setup the engine and creat the main window.
            Game.Create(Size.Font, Size.Width, Size.Height);

            // Hook the start event so we can add consoles to the system.
            Game.OnInitialize = Init;

            // Hook the update event that happens each frame so we can trap keys and respond.
            Game.OnUpdate = Update;
                        
            // Start the game.
            Game.Instance.Run();

            // Code here will not run until the game window closes.
            Game.Instance.Dispose();
        }
        
        private static void Update(GameTime time)
        {
            // As an example, we'll use the F5 key to make the game full screen
            if (Global.KeyboardState.IsKeyReleased(Keys.F5))
            {
                SadConsole.Settings.ToggleFullScreen();
            }
        }
        
        private static void Init()
        {
            // Setup window
            Game.Instance.Window.Title = "Troll's Treasure";
            // Create Menu console
            InitMenu();

        }
        
        
        private static void InitMenu()
        {
            _mainMenu = new MainMenu(Size.Width, Size.Height);
            _mainMenu.QuitGame += QuitMain;
            _mainMenu.StartGame += NewGame;
            _mainMenu.ContinueGame += ContinueGame;
            _mainMenu.Test += TestConsole;
            
            Global.CurrentScreen = _mainMenu;
            Global.FocusedConsoles.Set(_mainMenu);
        }
        
        // Load Consoles events 
        
        private static void MenuGamePlayConsole(object sender, EventArgs args)
        {
            InitMenu();
        }

        private static void TestConsole(object sender, EventArgs args)
        {
            var testConsole = new ControlsTest();
            testConsole.ToMenu += MenuGamePlayConsole;
            Global.CurrentScreen = testConsole;
            Global.FocusedConsoles.Set(testConsole);
        }
       
        private static void NewGame(object sender, EventArgs args)
        {
            _game = new GamePlay(Size.Width, Size.Height);
            _game.StopGamePlay += MenuGamePlayConsole;

            Global.CurrentScreen = _game;
            Global.FocusedConsoles.Set(_game);
        }
        
        private static void ContinueGame(object sender, EventArgs args)
        {
            Global.CurrentScreen = _game;
            Global.FocusedConsoles.Set(_game);
        }

        
        private static void QuitMain(object sender, EventArgs args)
        {
            // TODO add confirmation
            Game.Instance.Exit();
        }

    }
}
