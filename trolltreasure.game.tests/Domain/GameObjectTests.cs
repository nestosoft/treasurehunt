﻿using System.Collections.Generic;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using Xunit;

namespace trolltreasure.game.tests.Domain
{
    public class GameObjectTests
    {
        [Fact]
        public void ItShouldSortCorrectlyTheObjects()
        {
            var pos = new Position(1,2);
            var pos2 = new Position(1,3);
            var g1 = new GameObject(pos, ObjectType.Human, "g1");
            var g2 = new GameObject(pos, ObjectType.Human, "g2");
            g2.Fighter = new Fighter(10, 1, 1, 1);
            var g3 = new GameObject(pos, ObjectType.Human, "g3");
            g3.Fighter = new Fighter(15, 1, 3, 1);
            var g4 = new GameObject(pos2, ObjectType.Human, "g4");
            var list = new List<GameObject> {g3, g2, g1, g4};
            
            list.Sort();

            Assert.True(list[0] == g1);
            Assert.True(list[1] == g4);
            Assert.True(list[2] == g2);
            Assert.True(list[3] == g3);
        }
    }
}