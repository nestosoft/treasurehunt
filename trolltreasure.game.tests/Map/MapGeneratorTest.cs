﻿using trolltreasure.game.Domain;
using trolltreasure.game.Services;
using trolltreasure.game.Services.Factories;
using trolltreasure.game.Utils;
using Xunit;

namespace trolltreasure.game.tests.Map
{
    public class MapGeneratorTest
    {
        private readonly MapEmptyFactory MapGenerator;

        public MapGeneratorTest()
        {
            MapGenerator = new MapEmptyFactory();
        }
        
        
        [Theory]
        [InlineData(2, 4)]
        [InlineData(12, 4)]
        [InlineData(2, 14)]
        [InlineData(1, 1)]
        public void GenerateMap(int x, int y)
        {
            MapGenerator.GenerateMap(x, y);
            var map = MapGenerator.Map;
            Assert.Equal(x, map.GetLength(0));
            Assert.Equal(y, map.GetLength(1));
        }

        [Theory]
        [InlineData(1,1)]
        [InlineData(1,2)]
        public void CanAddWall(int x, int y)
        {
            MapGenerator.GenerateMap(4, 4);
            var map = MapGenerator.Map;
            var room = new Room(-1, -1, 5, 5); // all map is a room
            MapGeneration.CreateRoom(map, room);
            
            Assert.False(map[x, y].Blocked);
            Assert.False(map[x, y].BlockSight);
            
            MapGeneration.AddWall(map, x, y);
            
            Assert.True(map[x, y].Blocked);
            Assert.True(map[x, y].BlockSight);
        }

        [Theory]
        [InlineData(1,1,2,2)]
        [InlineData(0,0,2,2)]
        public void CanAddRoom(int x1, int y1, int x2, int y2)
        {
            MapGenerator.GenerateMap(4, 4);
            var map = MapGenerator.Map;
            var room = new Room(x1, y1, x2, y2);
            var center = room.Center();
            
            MapGeneration.CreateRoom(map, room);
           
            Assert.False(map[center.X, center.Y].Blocked); // middle of the smaller room 
            Assert.True(map[x1, y1].Blocked); 
            Assert.True(map[x1+x2, y1+y2].Blocked); // each corner of the room (remember x2 y2 are the width and height of the room)
            Assert.True(map[x1, y1+y2].Blocked); // each corner of the room (remember x2 y2 are the width and height of the room)
            Assert.True(map[x1+x2, y1].Blocked); // each corner of the room (remember x2 y2 are the width and height of the room)
        }
    }
}