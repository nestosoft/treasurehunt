﻿using System.Collections.Generic;
using System.Drawing;
using trolltreasure.game.Domain;
using trolltreasure.game.Services;
using trolltreasure.game.Services.Factories;
using trolltreasure.game.Services.Fov;
using Xunit;

namespace trolltreasure.game.tests.Services
{
    public class FovServiceTests
    {
        private Tile[,] map;
        private RecursiveShadowCasting _recursiveShadowCasting;
        private static readonly Point[] ExpectedListOneOne = {new Point(0, 0), new Point(0, 1), new Point(0,2), new Point(1,0), new Point(1,1), new Point(1,2), new Point(1,3), new Point(2,0), new Point(2,1), new Point(2,2)};
        private static readonly Point[] ExpectedListOneTwo = {new Point(1, 0), new Point(1, 0), new Point(1,1), new Point(1,2), new Point(0,2), new Point(1,2), new Point(2,2), new Point(0,3), new Point(1,3), new Point(2,3), new Point(1, 4)};
        private static readonly Point[] ExpectedLisAll = {new Point(0, 0), new Point(0, 1), new Point(0,2), new Point(0,3), new Point(0,4), new Point(0,5),  new Point(1,0), new Point(1,1), new Point(1,2), new Point(1,3), new Point(1,4), new Point(1,5), new Point(2,0), new Point(2,1), new Point(2,2), new Point(2,3), new Point(2,4), new Point(2,5), };

        public FovServiceTests()
        {
            
            var tesMapGen = new TestMapGenerator();
            tesMapGen.GenerateMap(4,6);
            map = tesMapGen.Map;
            
        }


        /*
         * Map is like:
         *  0123
         * 0####
         * 1# ##
         * 2# ##
         * 3# ##
         * 4# ##
         * 5####
         */
            
        
        [Fact]
        public void testFovForPositionOneOne()
        {
            _recursiveShadowCasting = new RecursiveShadowCasting(map, 2);
            List<Point> visible = _recursiveShadowCasting.GetVisibleCells(1, 1);
            Assert.Equal(ExpectedListOneOne.Length, visible.Count);
            foreach (var point in ExpectedListOneOne)
            {
                Assert.True(visible.Contains(point));
            }
        }
        
        [Fact]
        public void testFovForPositionOneTwo()
        {
            _recursiveShadowCasting = new RecursiveShadowCasting(map, 2);
            List<Point> visible = _recursiveShadowCasting.GetVisibleCells(1, 2);
            Assert.Equal(ExpectedListOneTwo.Length, visible.Count);
            foreach (var point in ExpectedListOneTwo)
            {
                Assert.True(visible.Contains(point));
            }
        }
        
        [Theory]
        [InlineData(1,1)]
        [InlineData(1,2)]
        [InlineData(1,3)]
        [InlineData(1,4)]
        public void testFovForPositionSeeEverythingButBlocked(int x, int y)
        {
            // TODO bug returning points (1,3)... behind a wall
            _recursiveShadowCasting = new RecursiveShadowCasting(map, 5);
            List<Point> visible = _recursiveShadowCasting.GetVisibleCells(x, y);
            Assert.Equal(ExpectedLisAll.Length, visible.Count);
            foreach (var point in ExpectedLisAll)
            {
                Assert.True(visible.Contains(point));
            }
        }
        
    }
}