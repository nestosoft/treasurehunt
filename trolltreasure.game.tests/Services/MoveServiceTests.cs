﻿using System.Collections.Generic;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Services;
using trolltreasure.game.Services.Factories;
using trolltreasure.game.Utils;
using Xunit;

namespace trolltreasure.game.tests.Services
{
    public class MoveServiceTests
    {
        private readonly MoveService _moveService;
        private readonly GameObject _testObject;
        private readonly MapEmptyFactory _mapGenerator;
        
        
        public MoveServiceTests()
        {
            _moveService = new MoveService();
            _testObject = new GameObject(new Position(0,0), ObjectType.Human, "test");
            _mapGenerator = new MapEmptyFactory();
        }
        
        [Theory]
        [InlineData(1, 1)]
        [InlineData(0, 1)]
        [InlineData(1, 0)]
        public void MoveObjectAroundTheMapFreely(int x, int y)
        {
            var map = GetWalkableMap();

            _moveService.MoveObject(_testObject, x, y, map, new List<GameObject>());
            
            Assert.Equal(x, _testObject.Position.X);
            Assert.Equal(y, _testObject.Position.Y);
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 0)]
        [InlineData(-1, 0)]
        [InlineData(0, -1)]
        [InlineData(-1, -1)]
        public void MoveObjectAroundTheMapCantGetOutTheMap(int x, int y)
        {
            _mapGenerator.GenerateMap(1, 1);
            var map = _mapGenerator.Map;
            _moveService.MoveObject(_testObject, x, y, map, new List<GameObject>());
            Assert.Equal(0, _testObject.Position.X); // initial position
            Assert.Equal(0, _testObject.Position.Y); // initial position
        }
        
        [Fact]
        public void MoveObjectCantTraspassWalls()
        {
            const int x = 1;
            const int y = 1;
            var map = GetWalkableMap();
            MapGeneration.AddWall(map, x, y);
            _moveService.MoveObject(_testObject, x, y, map, new List<GameObject>());
            Assert.Equal(0, _testObject.Position.X); // initial position
            Assert.Equal(0, _testObject.Position.Y); // initial position
        }

        [Fact]
        public void MoveObjectCantTraspassWallsObject()
        {
            const int x = 1;
            const int y = 1;
            var map = GetWalkableMap();
            map[x, y].Blocked = true;
            var player = new GameObject(new Position(0, 0),ObjectType.Human, "test", true);
            _moveService.MoveObject(_testObject, x, y, map, new List<GameObject>(){ player});
            Assert.Equal(0, _testObject.Position.X); // initial position
            Assert.Equal(0, _testObject.Position.Y); // initial position
            
            
        }
        
        [Fact]
        public void MoveObjectCantTraspassOtherObjects()
        {
            const int x = 1;
            const int y = 1;
            var map = GetWalkableMap();
            var npcPosition = new Position(x,y);
            var npc = new GameObject(npcPosition,ObjectType.Human, "test", true);
            _moveService.MoveObject(_testObject, npc.Position.X, npc.Position.Y, map, new List<GameObject>{ npc});
            Assert.Equal(0, _testObject.Position.X); // initial position
            Assert.Equal(0, _testObject.Position.Y); // initial position
            
        }
        
        
        
        [Fact]
        public void MoveObjectCanMoveOverNonBlocksObject()
        {
            const int x = 1;
            const int y = 1;
            var map = GetWalkableMap();
            _moveService.MoveObject(_testObject, x, y, map, new List<GameObject>(){ new GameObject(new Position(x, y), ObjectType.Human, "test")}); // false by default
            Assert.Equal(x, _testObject.Position.X); // initial position
            Assert.Equal(y, _testObject.Position.Y); // initial position
        }
        
        private Tile[,] GetWalkableMap()
        {
            _mapGenerator.GenerateMap(2, 2);
            var map = _mapGenerator.Map;
            var room = new Room(-1,-1, 3, 3); // all map is a room
            MapGeneration.CreateRoom(map, room);
            return map;
        }
        
    }
}