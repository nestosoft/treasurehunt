﻿using System.Collections.Generic;
using System.Linq;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Services.Factories;

namespace trolltreasure.game.Domain
{
    public class Dungeon
    {
        public readonly Map Map = new Map(50, 50);
        public readonly List<GameObject> Objects = new List<GameObject>();
        public GameObject Player;
        public int Level { get; }
        


        public Dungeon(int level)
        {
            // Initializing world of level
            Level = level;
            // initilise Entry Point
            var startPosition = Map.Rooms.First().Center();
            var stairsEntryPoint = new GameObject(startPosition, ObjectType.StairsUp, "Up");
            Objects.Add(stairsEntryPoint);
            // Initialise Monsters
            // populate monsters but not in the first room
            var roomsWithoutInitial = Map.Rooms.Skip(1);
            foreach (var room in roomsWithoutInitial)
            {
                Objects.AddRange(ObjectPlaceFactory.PlaceMonsters(room, Map.Tiles, level));
                Objects.AddRange(ObjectPlaceFactory.PlaceItems(room, Map.Tiles, level));   
            }
            
            // place exit
            var exitPosition = Map.Rooms.Last().Center();
            var stairsNewLevel = new GameObject(exitPosition, ObjectType.StairsDown, "Down");
            Objects.Add(stairsNewLevel);
        }

        public void PlacePlayer(GameObject player)
        {
            Player = player;
            Objects.Add(Player);
        }

        public Position EntryPosition()
        {
            return Objects.Find(o => o.ObjectType == ObjectType.StairsUp).Position.CopyPosition();
        }
        
        public Position ExitPosition()
        {
            return Objects.Find(o => o.ObjectType == ObjectType.StairsDown).Position.CopyPosition();
        }
    }
}