﻿namespace trolltreasure.game.Domain.Enums
{
    public enum EquipmentSlot
    {
        RightHand,
        LeftHand,
        Head,
        Legs,
        Back,
        Body
        
    }
}