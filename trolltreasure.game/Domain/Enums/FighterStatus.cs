﻿namespace trolltreasure.game.Domain.Enums
{
    public enum FighterStatus
    {
        Dead,
        Alive,
        Bleeding,
        Stunned,
        Critical,
        // more to come
    }
}