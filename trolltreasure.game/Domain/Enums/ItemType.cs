﻿namespace trolltreasure.game.Domain.Enums
{
    public enum ItemType
    {
        Potion,
        Scroll,
        Sword,
        Shield
    }
}