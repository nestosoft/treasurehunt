﻿namespace trolltreasure.game.Domain.Enums
{
    public enum MessageType
    {
        Cast,
        Inventory,
        ActionFailed,
        ActionSuccess,
        Information,
        Movement,
        Introduction,
        OnFire,
        AttackInformation
    }
}