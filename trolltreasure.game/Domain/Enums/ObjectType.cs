﻿namespace trolltreasure.game.Domain.Enums
{
    public enum ObjectType
    {
        Human,
        Troll,
        Orc,
        Item,
        StairsDown,
        StairsUp
    }
}