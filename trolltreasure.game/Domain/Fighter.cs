﻿using trolltreasure.game.Domain.Enums;

namespace trolltreasure.game.Domain
{
    public class Fighter
    {
        public int MaxHP;
        public int Defense;
        public int Power;
        public int HP;
        public int Xp;
        public FighterStatus Status;
        public int Level;
        
        public Fighter(int hp, int defense, int power, int xp)
        {
            MaxHP = hp;
            HP = hp;
            Defense = defense;
            Power = power;
            Xp = xp;
            Status = FighterStatus.Alive;
            Level = 1;
        }

        public int TakeDamage(int damage)
        {
            HP -= damage;
            return HP;
        }

        public void Heal(int amount)
        {
            HP += amount;
            if (HP > MaxHP) HP = MaxHP;
        }
       
        public FighterStatus CheckStatus()
        {
            if (HP <= 0) // Just died
            {
                Status = FighterStatus.Dead;
            }
            
            return Status;
        }
    }
}