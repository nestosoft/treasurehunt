﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Interfaces;

namespace trolltreasure.game.Domain
{
    public class GameObject: IComparable<GameObject>
    {
        public Position Position ;
        public ObjectType ObjectType;
        public string Name;
        public bool Blocks;
        public Fighter Fighter;
        public IBehavior Ai;
        public IFovService Fov;
        public Item Item;
        public List<Point> Visible => Fov.GetVisibleCells(Position.X, Position.Y);
        public List<Point> Visited = new List<Point>();
        public List<Item> Items = new List<Item>();

        public GameObject(Position pos, ObjectType objectType, string name, bool blocks = false, Fighter fighter = null, IBehavior ai = null, IFovService fov = null, Item item = null)
        {
            ObjectType = objectType;
            Position = pos;
            Name = name;
            Blocks = blocks;
            Fighter = fighter;
            Ai = ai;
            Fov = fov;
            Item = item;
        }
        

        public void UpdateStatus()
        {
            if (Fighter?.CheckStatus() != FighterStatus.Dead) return;
            Ai = null; // to not take turns
            Blocks = false; // to not block the pass  
        }

        public int CompareTo(GameObject other)
        {
            if (Fighter == null && other.Fighter == null)
            {
                if (ObjectType < other.ObjectType)
                {
                    return -1;
                }
                return ObjectType > other.ObjectType ? 1 : 0;
            }
            if (Fighter == null) return -1;
            if (other.Fighter == null) return 1;
            if (Fighter.HP < other.Fighter.HP)
            {
                return -1;
            }
            if (Fighter.HP > other.Fighter.HP)
            {
                return 1;
            };
            return 0;
        }
        
        public List<Item> EquippedItems()
        {
            return Items.FindAll(i => i.Equipment != null && i.Equipment.IsEquipped).ToList();
        }
    }
}