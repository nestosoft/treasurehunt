﻿using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Interfaces;

namespace trolltreasure.game.Domain
{
    public class Item
    {
        public string Name;
        public ItemType Type;
        public IUsable Use;
        public IEquipable Equipment;

        public Item(string name, ItemType type, IUsable use = null, IEquipable equipment =  null)
        {
            Name = name;
            Type = type;
            Use = use;
            Equipment = equipment;
        }
    }
}