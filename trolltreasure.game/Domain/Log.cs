﻿using trolltreasure.game.Domain.Enums;

namespace trolltreasure.game.Domain
{
    public class Log
    {
        public string Message;
        public MessageType Type;

        public Log(string message , MessageType type)
        {
            Message = message;
            Type = type;
        }
    }
}