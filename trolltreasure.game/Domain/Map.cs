﻿using System.Collections.Generic;
using System.Drawing;
using trolltreasure.game.Services;
using trolltreasure.game.Services.Factories;

namespace trolltreasure.game.Domain
{
    public class Map
    {
        public readonly Tile[,] Tiles;
        public readonly List<Room> Rooms;
        private readonly MapFactory _mapGenerator = new MapFactory();

        public Map(int width, int height)
        {
            _mapGenerator.GenerateMap(width, height);
            Tiles = _mapGenerator.Map;
            Rooms = _mapGenerator.Rooms;
        }
    }
}