﻿using System;

namespace trolltreasure.game.Domain
{
    public class Position
    {
        public int X;
        public int Y;

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }
        
        public int Distance(Position position)
        {
            //vector from this object to the target, and distance
            var dx = position.X - X;
            var dy = position.Y - Y;
            var dis = Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));
            return (int) Math.Ceiling(dis);
        }

        public bool Equals(Position other)
        {
            return X == other.X && Y == other.Y;
        }

        public Position Diff(Position otherPosition)
        {
            var dx = X - otherPosition.X;
            var dy = Y - otherPosition.Y;
            return new Position(dx, dy);
        }

        public Position CopyPosition()
        {
            return new Position(X, Y);
        }

        public override string ToString()
        {
            return $"({X},{Y})";
        }
    }
}