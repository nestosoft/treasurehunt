﻿namespace trolltreasure.game.Domain.Results
{
    public class DamageResult : Result
    {
        public int Damage;
    }
}