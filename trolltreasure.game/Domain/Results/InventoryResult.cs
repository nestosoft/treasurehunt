﻿namespace trolltreasure.game.Domain.Results
{
    public class InventoryResult : Result
    {
        public bool Used = false;
    }
}