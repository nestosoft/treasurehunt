﻿using System.Collections.Generic;

namespace trolltreasure.game.Domain.Results
{
    public class Result
    {
        public readonly List<Log> Logs = new List<Log>();
    }
}