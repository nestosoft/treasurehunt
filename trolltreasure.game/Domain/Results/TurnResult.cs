﻿using System.Collections.Generic;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.game.Domain
{
    public class TurnResult : Result
    {
        public readonly List<GameObject> ObjectsToDraw = new List<GameObject>();
        
    }
}