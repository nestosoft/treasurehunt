﻿using System;

namespace trolltreasure.game.Domain
{
    public class Room
    {
        public readonly int X1;
        public readonly int Y1;
        public readonly int X2;
        public readonly int Y2;
            
            
        public Room(int x, int y, int width, int height)
        {
            X1 = x;
            Y1 = y;
            X2 = x + width;
            Y2 = y + height;
        }

        public Position Center()
        {
            var centerX = (X1 + X2) / 2;  
            var centerY = (Y1 + Y2) / 2;  
            return new Position(centerX, centerY);
        }

        public bool Intersect(Room other)
        {
            return X1 <= other.X2 && X2 >= other.X1 && Y1 <= other.Y2 && Y2 >= other.Y1;
        }
    }
}