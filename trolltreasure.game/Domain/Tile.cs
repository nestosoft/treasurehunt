﻿namespace trolltreasure.game.Domain
{
    public class Tile
    {
        public bool BlockSight { get; set; }
        public bool Blocked { get; set; }

        public Tile(bool blocked, bool blockSight = false)
        {
            Blocked = blocked;
            BlockSight = blockSight || blocked;
        }
    }
}