﻿using System.Collections.Generic;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.game.Interfaces
{
    public interface IBehavior
    {
        Result TakeTurn(GameObject objToDoTurn , Tile[,] map, List<GameObject> otherObjectsThatInfluenceDecision);
    }
}