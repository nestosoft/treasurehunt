﻿using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.game.Interfaces
{
    public interface IEquipable
    {
        InventoryResult ToggleEquip(GameObject obj, Item item);
        InventoryResult Equip(GameObject obj, Item item);
        InventoryResult Dequip(GameObject obj, Item item);
        Item ItemEquippedInSlot(GameObject obj, EquipmentSlot slot);
        bool IsEquippedIn(EquipmentSlot slot);

        bool IsEquipped { get; }
        EquipmentSlot Slot { get; }

        int PowerBonus { get; }
        int DefenseBonus { get; }
    }
}