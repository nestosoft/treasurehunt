﻿using System.Collections.Generic;
using System.Drawing;

namespace trolltreasure.game.Interfaces
{
    public interface IFovService
    {
        List<Point> GetVisibleCells(int x, int y);
        bool IsPositionVisible(int x, int y);
        int Awareness { get; set; }
    }
}