﻿using System.Collections.Generic;
using trolltreasure.game.Domain;

namespace trolltreasure.game.Interfaces
{
    public interface IMapFactory
    {
        Tile[,] Map { get; }
        List<Room> Rooms { get; }
        
        void GenerateMap(int width, int height);
        
    }
}