﻿using System.Collections.Generic;
using trolltreasure.game.Domain;

namespace trolltreasure.game.Interfaces
{
    public interface IMoveService
    {
        void MoveObject(GameObject obj, int dx, int dy, Tile[,] map,  List<GameObject> objects);
    }
}