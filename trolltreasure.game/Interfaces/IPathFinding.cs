﻿using System.Collections.Generic;
using trolltreasure.game.Domain;

namespace trolltreasure.game.Interfaces
{
    public interface IPathFinding
    {
        List<Position> Path(Position from, Position to, Tile[,] map, List<Position> objects);
        Position NextMove(Position from, Position to, Tile[,] map, List<Position> objectPostiPositions);
    }
}