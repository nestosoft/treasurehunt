﻿using System;
using System.Collections.Generic;
using System.Drawing;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.game.Interfaces
{
    public interface IUsable
    {
        bool Targetable { get; }
        
        InventoryResult UseBy(GameObject caster, Position target, Dungeon dungeon);
    }
}