﻿using System.Linq;
using System.Security.Permissions;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.game.Services
{
    public class AttackService
    {
        public DamageResult CalculateDamage(GameObject attacker, GameObject defender)
        {
            var result = new DamageResult();
            var damage = attacker.Fighter.Power - defender.Fighter.Defense;
            result.Logs.Add(new Log($"Base Damage {damage}", MessageType.AttackInformation));
            var attackerEquippedItems = attacker.EquippedItems();
            if (attackerEquippedItems.Count > 0)
            {
                var bonusAttack = attackerEquippedItems.Sum(item => item.Equipment.PowerBonus);
                result.Logs.Add(new Log($"Total Bonus Attack {bonusAttack}", MessageType.AttackInformation));
                damage += bonusAttack;
                
            }

            var defenderEquippedItems = defender.EquippedItems();
            if (defenderEquippedItems.Count > 0)
            {
                var bonusDefense = defenderEquippedItems.Sum(item => item.Equipment.DefenseBonus);
                result.Logs.Add(new Log($"Total Bonus Defense {bonusDefense}", MessageType.AttackInformation));
                damage -= bonusDefense;
            }
            
            result.Logs.Add(new Log($"Total Damage {damage}", MessageType.AttackInformation));
            
            if (damage > 0)
            {
                defender.Fighter.TakeDamage(damage);
                result.Logs.Add(new Log($"{attacker.Name} infict {damage} HP of damage to {defender.Name}", MessageType.ActionSuccess));
                result.Damage = damage;
            }

            return result;
        }
    }
}