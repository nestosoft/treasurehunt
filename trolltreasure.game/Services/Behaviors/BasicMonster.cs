﻿using System.Collections.Generic;
using System.Linq;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;
using trolltreasure.game.Interfaces;
using trolltreasure.game.Services.Movement;
using trolltreasure.game.Utils;

namespace trolltreasure.game.Services.Behaviors
{
    public class BasicMonster : IBehavior
    {
        private readonly MoveService _moveService = new MoveService();
        private readonly AttackService _attackService = new AttackService();
        //private readonly IPathFinding _pathService = new MoveService();
        private readonly IPathFinding _pathService = new AStarPathFinding();
        
        public Result TakeTurn(GameObject obj, Tile[,] map, List<GameObject> objectsPosition)
        {
            var result = new Result();
            // recalculate FOV
            obj.Fov.GetVisibleCells(obj.Position.X, obj.Position.Y);
            
            // in this case I find all the humans and go for them. Monsters dont like humans
            var humans = objectsPosition.FindAll(x => x.ObjectType == ObjectType.Human);
            GameObject target = null;
            double distance = obj.Fov.Awareness;
            foreach (var human in humans)
            {
                if (obj.Fov.IsPositionVisible(human.Position.X, human.Position.Y) &&
                obj.Position.Distance(human.Position) < distance)
                {
                    target = human;
                    distance = obj.Position.Distance(human.Position);
                }
            }
            
            // ok, hopefully we have a target there .... TODO: check last target position for when the player hide
            if (target != null)
            {
                if (distance >= 2)
                {
                    var prevX = obj.Position.X;
                    var prevY = obj.Position.Y;
                    var nextPosition = _pathService.NextMove(obj.Position, target.Position, map, objectsPosition.Select(o => o.Position).ToList());
                    _moveService.MoveObject(obj, nextPosition.X, nextPosition.Y, map, objectsPosition);
                    result.Logs.Add(new Log($"Monster {obj.Name} moved ({prevX},{prevY}) -> ({obj.Position.X},{obj.Position.Y})", MessageType.Movement));
                }
                else if (target.Fighter.HP > 0)
                {
                    var damageResult = _attackService.CalculateDamage(obj, target);
                    result.Logs.AddRange(damageResult.Logs);
                    result.Logs.Add(new Log($"Monster {obj.Name} attack the human {target.Name} and make {damageResult.Damage} damage", MessageType.ActionSuccess) );
                    result.Logs.Add(new Log($"the {target.Name} has {target.Fighter.HP} HP remaining", MessageType.Information));
                }
            }

            return result;
        }
    }
}