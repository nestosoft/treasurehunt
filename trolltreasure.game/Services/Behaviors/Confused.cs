﻿using System.Collections.Generic;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;
using trolltreasure.game.Interfaces;
using trolltreasure.game.Utils;

namespace trolltreasure.game.Services.Behaviors
{
    public class Confused : IBehavior
    {
        private readonly MoveService _moveService = new MoveService();
        private readonly IBehavior _oldBehavior;
        private int _numberOfTurns;
        private const int ConfusedTurns = 10;

        public Confused(IBehavior oldBehavior, int numberOfTurns = ConfusedTurns)
        {
            _numberOfTurns = numberOfTurns;
            _oldBehavior = oldBehavior;
        }
        
        public Result TakeTurn(GameObject objToDoTurn, Tile[,] map, List<GameObject> otherObjectsThatInfluenceDecision)
        {
            var result = new Result();
            if (_numberOfTurns > 0)
            {
                var randomX = RandomGeneration.GetNextRandomBetween(-1, 1);
                var randomY = RandomGeneration.GetNextRandomBetween(-1, 1);
                _moveService.MoveObject(objToDoTurn, randomX, randomY, map, otherObjectsThatInfluenceDecision);
                _numberOfTurns -= 1;
                result.Logs.Add(new Log($"The {objToDoTurn.Name} is confused!", MessageType.Information));
                return result;
            }
            
            objToDoTurn.Ai = _oldBehavior;
            result.Logs.Add(new Log( $"The {objToDoTurn.Name} is no longer confused!", MessageType.ActionSuccess));
            return result;
        }
    }
}