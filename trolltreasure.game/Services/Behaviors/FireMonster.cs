﻿using System;
using System.Collections.Generic;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Results;
using trolltreasure.game.Interfaces;

namespace trolltreasure.game.Services.Behaviors
{
    public class FireMonster : IBehavior
    {
        private State state = State.Idle;


        public Result TakeTurn(GameObject objToDoTurn, Tile[,] map, List<GameObject> otherObjectsThatInfluenceDecision)
        {
            switch (state)
            {
                case State.Idle:
                    // move randmly
                    break;
                case State.Chasing:
                    // chase the target
                    break;
                case State.ChargingFire:
                    // one turn to be ready to throw a fireball
                    break;
                case State.ReadyToAttack:
                    // Attack target (cast fireball)
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return new Result();
        }
    }

    internal enum State
    {
        Idle,
        ChargingFire,
        ReadyToAttack,
        Chasing
    }
}