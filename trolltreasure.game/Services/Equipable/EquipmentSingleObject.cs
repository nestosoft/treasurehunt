﻿using System.Collections.Generic;
using System.Linq;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;
using trolltreasure.game.Interfaces;

namespace trolltreasure.game.Services.Equipable
{
    public class EquipmentSingleObject : IEquipable
    {
        private bool _isEquipped;
        public bool IsEquipped => _isEquipped;
        public EquipmentSlot Slot { get; }
        public int PowerBonus { get; }
        public int DefenseBonus { get; }

        public EquipmentSingleObject(EquipmentSlot slot, int powerBonus = 0, int defenseBonus = 0)
        {
            Slot = slot;
            PowerBonus = powerBonus;
            DefenseBonus = defenseBonus;
        }
        
        public InventoryResult ToggleEquip(GameObject obj, Item item)
        {
            return IsEquipped ? Dequip(obj, item) : Equip(obj, item);
        }

        public InventoryResult Equip(GameObject obj, Item item)
        {
            if (item?.Equipment != null)
            {
                var itemInSlot = ItemEquippedInSlot(obj, Slot);
                InventoryResult dequipResult = null;
                if (itemInSlot != null)
                {
                    dequipResult = Dequip(obj, itemInSlot);
                    dequipResult.Logs.Add(new Log($"The {itemInSlot.Name} has been replaced with {item.Name} in the {Slot}.", MessageType.Inventory));
                }
                
                _isEquipped = true;
                var result = new InventoryResult();
                if (dequipResult != null ) result.Logs.AddRange(dequipResult.Logs);
                result.Logs.Add( new Log($"Equipped {item.Name} on {Slot}.", MessageType.Inventory));
                return result;
            }


            return new InventoryResult
            {
                Logs = {new Log($"Item {item.Name} Cannot be equipped.", MessageType.Inventory)}
            };
        }

        public InventoryResult Dequip(GameObject obj, Item item)
        {
            if (item?.Equipment != null)
            {
                if (ItemEquippedInSlot(obj, Slot) != null)
                {
                    _isEquipped = false;
                    return new InventoryResult { Logs = { new Log($"Dequipped {item.Name} on {Slot}.", MessageType.Inventory)}};
                }
                return new InventoryResult { Logs = { new Log($"The slot {Slot} is already empty.", MessageType.Inventory)}};
            }

            
            return new InventoryResult
            {
                Logs = {new Log($"Item {item.Name} Cannot be equipped.", MessageType.Inventory)}
            };
        }


        public bool IsEquippedIn(EquipmentSlot slot)
        {
            return slot == Slot;
        }

        
        
        public Item ItemEquippedInSlot(GameObject obj, EquipmentSlot slot)
        {
            var listEquiItems = obj.EquippedItems();
            return listEquiItems.FirstOrDefault(item => item.Equipment.IsEquippedIn(slot));
        }
    }
}