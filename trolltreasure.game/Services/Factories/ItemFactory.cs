﻿using System.Collections.Generic;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Services.Equipable;
using trolltreasure.game.Services.Usable;

namespace trolltreasure.game.Services.Factories
{
    public static class ItemFactory
    {
        public static GameObject HealingPotion(Position position)
        {
            const string name = "Healing Potion";
            var potion = new GameObject(position, ObjectType.Item, name)
            {
                Item = new Item(name, ItemType.Potion, new CastHeal())
            };
            return potion;
        }
        
        public static GameObject Sword(Position position)
        {
            const string name = "Sword";
            var sword = new GameObject(position, ObjectType.Item, name)
            {
                Item = new Item(name, ItemType.Sword, null, new EquipmentSingleObject(EquipmentSlot.RightHand, powerBonus:2))
            };
            return sword;
        }
        
        public static GameObject Shield(Position position)
        {
            const string name = "Shield";
            var shield = new GameObject(position, ObjectType.Item, name)
            {
                Item = new Item(name, ItemType.Shield, null, new EquipmentSingleObject(EquipmentSlot.LeftHand, defenseBonus:2))
            };
            return shield;
        }
        
        
        public static GameObject LightningBoltScroll(Position position)
        {
            const string name = "Scroll of lightning bolt";
            var scroll = new GameObject(position, ObjectType.Item, name)
            {
                Item = new Item(name, ItemType.Scroll, new CastLightning())
            };
            return scroll;
        }

        public static GameObject ConfusionScroll(Position position)
        {
            const string name = "Scroll of Confusion";
            var scroll = new GameObject(position, ObjectType.Item, name)
            {
                Item = new Item(name, ItemType.Scroll, new CastConfusion())
            };
            return scroll;
        }
        
        public static GameObject FireballScroll(Position position)
        {
            const string name = "Scroll of Fireball";
            var scroll = new GameObject(position, ObjectType.Item, name)
            {
                Item = new Item(name, ItemType.Scroll, new CastFireball())
            };
            return scroll;
        }
    }
}