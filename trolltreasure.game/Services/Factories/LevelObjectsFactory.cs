﻿using System.Collections.Generic;

namespace trolltreasure.game.Services.Factories
{
    public static class LevelObjectsFactory
    {
        private static readonly int[,] MaxMonsterTable = {{1,2},{4,3},{6,5}}; 
        private static readonly int[,] MaxItemTable = {{1,2},{4,3}}; 
        private static readonly int[,] MaxTrollTable = {{1,2},{4,3},{6,5}}; 
        private static readonly int[,] MaxLightningTable = {{1,10},{4,25}}; 
        private static readonly int[,] MaxFireballTable = {{6,25}}; 
        private static readonly int[,] MaxConfuseTable = {{1,30},{2,10}};

        public static int MaxMonster(int level)
        {
            return FromLevel(level, MaxMonsterTable);
        }
        
        public static int MaxItem(int level)
        {
            return FromLevel(level, MaxItemTable);
        }
        
        public static Dictionary<string, int> MonsterChances(int level)
        {
            var chances = new Dictionary<string, int>
            {
                {"orc", 80}, //orc always shows up, even if all other monsters have 0 chance
                {"troll", FromLevel(level, MaxTrollTable)}
            };
            
            return chances;
        }
        
        public static Dictionary<string, int> ItemChances(int level)
        {
            var chances = new Dictionary<string, int>
            {
                {"sword", 50},
                {"shield", 50},
                {"heal", 50},
                {"lightning", FromLevel(level, MaxLightningTable)},
                {"fireball", FromLevel(level, MaxFireballTable)},
                {"confuse", FromLevel(level, MaxConfuseTable)}
            };
            return chances;
        }

        private static int FromLevel(int level, int[,] table)
        {
            for (var i = table.GetLength(0)-1; i >= 0; i--)
            {
                var l = table[i, 0];
                var v = table[i, 1];
                if (level >= l) return v;
            }
            return 0;

        }
    }
}