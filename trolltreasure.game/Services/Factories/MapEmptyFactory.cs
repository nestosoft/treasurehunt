﻿using System.Collections.Generic;
using trolltreasure.game.Domain;
using trolltreasure.game.Interfaces;

namespace trolltreasure.game.Services.Factories
{
    public class MapEmptyFactory : IMapFactory
    {
        public Tile[,] Map { get; set; }
        public List<Room> Rooms { get; set; }

        public void GenerateMap(int width, int height)
        {
            Map = new Tile[width, height];
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    Map[i,j] = new Tile(true);
                }
            }
        }
    }
}