﻿using System.Collections.Generic;
using System.Linq;
using trolltreasure.game.Domain;
using trolltreasure.game.Interfaces;
using trolltreasure.game.Utils;

namespace trolltreasure.game.Services.Factories
{
    public class MapFactory : IMapFactory
    {
        private List<Room> _rooms = new List<Room>();
        private const int RoomMaxSize = 10;
        private const int RoomMinSize = 6;
        private const int MaxRooms = 30;

        public Tile[,] Map { get; set; }
        public List<Room> Rooms
        {
            get => _rooms;
            set => _rooms = value;
        }

        public void GenerateMap(int width, int height)
        {
            Map = new Tile[width, height];
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    Map[i,j] = new Tile(true);
                }
            }
            
            Room lastRoom = null;

            for (var r = 0; r <= MaxRooms; r++)
            {
                var w = RandomGeneration.GetNextRandomBetween(RoomMinSize, RoomMaxSize);
                var h = RandomGeneration.GetNextRandomBetween(RoomMinSize, RoomMaxSize);

                var x = RandomGeneration.GetNextRandomBetween(0, Map.GetLength(0) - w - 1);
                var y = RandomGeneration.GetNextRandomBetween(0, Map.GetLength(1) - h - 1);
                
                var newRoom = new Room(x, y, w, h);
                
                // Check if there is another intersecting room
                var failed = Rooms.Any(other => newRoom.Intersect(other));
                if (!failed)
                {
                    MapGeneration.CreateRoom(Map, newRoom);

                    if (lastRoom == null)
                    {
                        lastRoom = newRoom;
                    }
                    else
                    {
                        var center = newRoom.Center();
                        var prevCenter = lastRoom.Center();

                        if (RandomGeneration.GetNextRandomBetween(0, 1) == 1)
                        {
                            MapGeneration.CreateHorizontalTunnel(Map, prevCenter.X, center.X, prevCenter.Y);
                            MapGeneration.CreateVerticalTunnel(Map, prevCenter.Y, center.Y, center.X);
                        }
                        else
                        {
                            MapGeneration.CreateVerticalTunnel(Map, prevCenter.Y, center.Y, prevCenter.X);
                            MapGeneration.CreateHorizontalTunnel(Map, prevCenter.X, center.X, center.Y);
                        }
                    }
                    
                    _rooms.Add(newRoom);
                }
            }
        }
    }
}