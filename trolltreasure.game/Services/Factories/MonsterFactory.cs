﻿using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Services.Behaviors;
using trolltreasure.game.Services.Fov;

namespace trolltreasure.game.Services.Factories
{
    public static class MonsterFactory
    {
        public static GameObject Player(Position position, string name, Tile[,] map)
        {
            return new GameObject(position, ObjectType.Human, name, true)
            {
                Fighter = new Fighter(100, 1, 4, 0),
                Fov = new ShadowCaster(map, 5)
            };
        }
        
        public static GameObject Orc(Position position, string name, Tile[,] map)
        {
            var orc = new GameObject(position, ObjectType.Orc, name, true);
            var fighter = new Fighter(20, 0, 4, 35);
            orc.Fighter = fighter;
            var ai = new BasicMonster();
            orc.Ai = ai;
            var fov = new ShadowCaster(map, 5);
            orc.Fov = fov;
            return orc;
        }
        
        public static GameObject Troll(Position position, string name, Tile[,] map)
        {
            var troll = new GameObject(position, ObjectType.Troll, name, true);
            var fighter = new Fighter(30, 2, 8, 100);
            troll.Fighter = fighter;
            var ai = new BasicMonster();
            troll.Ai = ai;
            var fov = new ShadowCaster(map, 5);
            troll.Fov = fov;
            return troll;
        }
    }
}