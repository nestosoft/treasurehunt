﻿using System.Collections.Generic;
using System.Linq;
using trolltreasure.game.Domain;
using trolltreasure.game.Utils;

namespace trolltreasure.game.Services.Factories
{
    public static class ObjectPlaceFactory
    {
        public static IEnumerable<GameObject> PlaceMonsters(Room room, Tile[,] map, int level)
        {
            var list = new List<GameObject>();
            var numMonster = RandomGeneration.GetNextRandomBetween(0, LevelObjectsFactory.MaxMonster(level));

            for (var i = 0; i < numMonster; i++)
            {
                var positionX = RandomGeneration.GetNextRandomBetween(room.X1+1, room.X2-1);
                var positionY = RandomGeneration.GetNextRandomBetween(room.Y1+1, room.Y2-1);
                var position = new Position(positionX, positionY);
                if (map[positionX, positionY].Blocked ||
                    list.Any(o => o.Blocks && o.Position.Equals(position))) continue;
                
                var choice = RandomGeneration.RandomChoice(LevelObjectsFactory.MonsterChances(level));
                if (choice ==  "orc")
                {
                    var orc = MonsterFactory.Orc(position, $"Orc-{i}", map);
                    list.Add(orc);
                }
                else if (choice == "troll")
                {
                    var troll = MonsterFactory.Troll(position, $"Troll-{i}", map);
                    list.Add(troll);
                }
            }

            return list;
        }

        public static IEnumerable<GameObject> PlaceItems(Room room, Tile[,] map, int level)
        {
            var list = new List<GameObject>();
            var items = RandomGeneration.GetNextRandomBetween(0, LevelObjectsFactory.MaxItem(level));

            for (var i = 0; i < items; i++)
            {
                var positionX = RandomGeneration.GetNextRandomBetween(room.X1 + 1, room.X2 - 1);
                var positionY = RandomGeneration.GetNextRandomBetween(room.Y1 + 1, room.Y2 - 1);
                var position = new Position(positionX, positionY);

                if (map[position.X, position.Y].Blocked) continue;
                var choice = RandomGeneration.RandomChoice(LevelObjectsFactory.ItemChances(level));
                if (choice == "heal")
                {
                    var item = ItemFactory.HealingPotion(position);
                    list.Add(item);
                }
                else if (choice == "sword")
                {
                    var item = ItemFactory.Sword(position);
                    list.Add(item);
                }
                else if (choice == "shield")
                {
                    var item = ItemFactory.Shield(position);
                    list.Add(item);
                }
                else if (choice == "lightning")
                {
                    var item = ItemFactory.LightningBoltScroll(position);
                    list.Add(item);
                }
                else if (choice == "confusion")
                {
                    var item = ItemFactory.ConfusionScroll(position);
                    list.Add(item);
                }
                else if (choice == "fireball")
                {
                    var item = ItemFactory.FireballScroll(position);
                    list.Add(item);
                }
            }
            return list;
        }
    }
}