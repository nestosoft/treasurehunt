﻿using System;
using System.Collections.Generic;
using trolltreasure.game.Domain;
using trolltreasure.game.Interfaces;

namespace trolltreasure.game.Services.Factories
{
    public class TestMapGenerator : IMapFactory
    {
        public Tile[,] Map { get => _map; }
        public List<Room> Rooms { get; }

        private Tile[,] _map;

        public void GenerateMap(int width, int height)
        {
            _map = new Tile[width, height];
            for (var j = 0; j < height; j++)
            {
                for (var i = 0; i < width; i++)
                {
                    if (i > 0 && i < width -2 && j != 0 && j != height-1)
                    {
                        _map[i, j] = new Tile(false);
                        System.Diagnostics.Debug.Write(' ');
                    }
                    else
                    {
                        _map[i, j] = new Tile(true);
                        System.Diagnostics.Debug.Write('#');
                    }
                     
                }
                System.Diagnostics.Debug.Write(Environment.NewLine);
            }
        }

    }
}