﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using trolltreasure.game.Domain;
using trolltreasure.game.Interfaces;

namespace trolltreasure.game.Services.Fov
{
    // From http://www.evilscience.co.uk/field-of-vision-using-recursive-shadow-casting-c-3-5-implementation/
    // has some kind of bug in the octant 5 that allow to see behind the walls (dont found why)
    public class RecursiveShadowCasting : IFovService
    {
        readonly List<int> _visibleOctants = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8 };
        private HashSet<Point> VisiblePoints { get; set; }
        private Tile[,] Map { get; }
        public int Awareness { get; set; }
        
        
        public RecursiveShadowCasting(Tile[,] map, int awareness)
        {
            Map = map;
            Awareness = awareness;
        }
        
        
        //  Octant data
        //
        //    \ 1 | 2 /
        //   8 \  |  / 3
        //   -----+-----
        //   7 /  |  \ 4
        //    / 6 | 5 \
        //
        //  1 = NNW, 2 =NNE, 3=ENE, 4=ESE, 5=SSE, 6=SSW, 7=WSW, 8 = WNW
        public List<Point> GetVisibleCells(int x, int y)
        {
            var position = new Point(x, y);
            VisiblePoints = new HashSet<Point> { position };
            foreach (var o in _visibleOctants)
                ScanOctant(1, o, 1.0, 0.0, position, Awareness);

            return VisiblePoints.ToList();
        }

        public bool IsPositionVisible(int x, int y)
        {
            return VisiblePoints.Contains(new Point(x,y));
        }

        /// <summary>
        /// Examine the provided octant and calculate the visible cells within it.
        /// </summary>
        /// <param name="pDepth">Depth of the scan</param>
        /// <param name="pOctant">Octant being examined</param>
        /// <param name="pStartSlope">Start slope of the octant</param>
        /// <param name="pEndSlope">End slope of the octance</param>
        /// <param name="position">position from where to calculate</param>
        /// <param name="visualRange">visual range</param>
        protected void ScanOctant(int pDepth, int pOctant, double pStartSlope, double pEndSlope, Point position, int visualRange)
        {

            int visrange2 = visualRange * visualRange;
            int x = 0;
            int y = 0;

            switch (pOctant)
            {

                case 1: //nnw
                    y = position.Y - pDepth;
                    if (y < 0) return;

                    x = position.X - Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
                    if (x < 0) x = 0;

                    while (GetSlope(x, y, position.X, position.Y, false) >= pEndSlope)
                    {
                        if (GetVisDistance(x, y, position.X, position.Y) <= visrange2)
                        {
                            if (Map[x, y].BlockSight) //current cell blocked
                            {
                                if (x - 1 >= 0 && !Map[x - 1, y].BlockSight) //prior cell within range AND open...
                                    //...incremenet the depth, adjust the endslope and recurse
                                    ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x - 0.5, y + 0.5, position.X, position.Y, false), position, visualRange);
                            }
                            else
                            {

                                if (x - 1 >= 0 && Map[x - 1, y].BlockSight) //prior cell within range AND open...
                                    //..adjust the startslope
                                    pStartSlope = GetSlope(x - 0.5, y - 0.5, position.X, position.Y, false);
                            }
                            
                            VisiblePoints.Add(new Point(x, y));
                        }
                        x++;
                    }
                    x--;
                    break;

                case 2: //nne

                    y = position.Y - pDepth;
                    if (y < 0) return;

                    x = position.X + Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
                    if (x >= Map.GetLength(0)) x = Map.GetLength(0) - 1;

                    while (GetSlope(x, y, position.X, position.Y, false) <= pEndSlope)
                    {
                        if (GetVisDistance(x, y, position.X, position.Y) <= visrange2)
                        {
                            if (Map[x, y].BlockSight)
                            {
                                if (x + 1 < Map.GetLength(0) && !Map[x + 1, y].BlockSight)
                                    ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x + 0.5, y + 0.5, position.X, position.Y, false), position, visualRange);
                            }
                            else
                            {
                                if (x + 1 < Map.GetLength(0) && Map[x + 1, y].BlockSight)
                                    pStartSlope = -GetSlope(x + 0.5, y - 0.5, position.X, position.Y, false);
                            }

                            VisiblePoints.Add(new Point(x, y));
                        }
                        x--;
                    }
                    x++;
                    break;

                case 3:

                    x = position.X + pDepth;
                    if (x >= Map.GetLength(0)) return;

                    y = position.Y - Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
                    if (y < 0) y = 0;

                    while (GetSlope(x, y, position.X, position.Y, true) <= pEndSlope)
                    {

                        if (GetVisDistance(x, y, position.X, position.Y) <= visrange2)
                        {

                            if (Map[x, y].BlockSight)
                            {
                                if (y - 1 >= 0 && !Map[x, y - 1].BlockSight)
                                    ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x - 0.5, y - 0.5, position.X, position.Y, true), position, visualRange);
                            }
                            else
                            {
                                if (y - 1 >= 0 && Map[x, y - 1].BlockSight)
                                    pStartSlope = -GetSlope(x + 0.5, y - 0.5, position.X, position.Y, true);
                            }
                            
                            VisiblePoints.Add(new Point(x, y));
                        }
                        y++;
                    }
                    y--;
                    break;

                case 4:

                    x = position.X + pDepth;
                    if (x >= Map.GetLength(0)) return;

                    y = position.Y + Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
                    if (y >= Map.GetLength(1)) y = Map.GetLength(1) - 1;

                    while (GetSlope(x, y, position.X, position.Y, false) >= pEndSlope)
                    {

                        if (GetVisDistance(x, y, position.X, position.Y) <= visrange2)
                        {

                            if (Map[x, y].BlockSight)
                            {
                                if (y + 1 < Map.GetLength(1) && !Map[x, y + 1].BlockSight)
                                    ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x - 0.5, y + 0.5, position.X, position.Y, true), position, visualRange);
                            }
                            else
                            {
                                if (y + 1 < Map.GetLength(1) && Map[x, y + 1].BlockSight)
                                    pStartSlope = GetSlope(x + 0.5, y + 0.5, position.X, position.Y, true);
                            }
                            
                            VisiblePoints.Add(new Point(x, y));
                        }
                        y--;
                    }
                    y++;
                    break;

                case 5:

                    y = position.Y + pDepth;
                    if (y >= Map.GetLength(1)) return;

                    x = position.X + Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
                    if (x >= Map.GetLength(0)) x = Map.GetLength(0) - 1;

                    while (GetSlope(x, y, position.X, position.Y, false) >= pEndSlope)
                    {
                        if (GetVisDistance(x, y, position.X, position.Y) <= visrange2)
                        {

                            if (Map[x, y].BlockSight)
                            {
                                if (x + 1 < Map.GetLength(1) && !Map[x + 1, y].BlockSight)
                                    ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x + 0.5, y - 0.5, position.X, position.Y, false), position, visualRange);
                            }
                            else
                            {
                                if (x + 1 < Map.GetLength(1) && Map[x + 1, y].BlockSight)
                                    pStartSlope = GetSlope(x + 0.5, y + 0.5, position.X, position.Y, false);
                            }

                            VisiblePoints.Add(new Point(x, y));
                        }
                        x--;
                    }
                    x++;
                    break;

                case 6:

                    y = position.Y + pDepth;
                    if (y >= Map.GetLength(1)) return;

                    x = position.X - Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
                    if (x < 0) x = 0;

                    while (GetSlope(x, y, position.X, position.Y, false) <= pEndSlope)
                    {
                        if (GetVisDistance(x, y, position.X, position.Y) <= visrange2)
                        {

                            if (Map[x, y].BlockSight)
                            {
                                if (x - 1 >= 0 && !Map[x - 1, y].BlockSight)
                                    ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x - 0.5, y - 0.5, position.X, position.Y, false), position, visualRange);
                            }
                            else
                            {
                                if (x - 1 >= 0
                                        && Map[x - 1, y].BlockSight)
                                    pStartSlope = -GetSlope(x - 0.5, y + 0.5, position.X, position.Y, false);
                            }

                            VisiblePoints.Add(new Point(x, y));
                        }
                        x++;
                    }
                    x--;
                    break;

                case 7:

                    x = position.X - pDepth;
                    if (x < 0) return;

                    y = position.Y + Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
                    if (y >= Map.GetLength(1)) y = Map.GetLength(1) - 1;

                    while (GetSlope(x, y, position.X, position.Y, true) <= pEndSlope)
                    {

                        if (GetVisDistance(x, y, position.X, position.Y) <= visrange2)
                        {

                            if (Map[x, y].BlockSight)
                            {
                                if (y + 1 < Map.GetLength(1) && !Map[x, y + 1].BlockSight)
                                    ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x + 0.5, y + 0.5, position.X, position.Y, true), position, visualRange);
                            }
                            else
                            {
                                if (y + 1 < Map.GetLength(1) && Map[x, y + 1].BlockSight)
                                    pStartSlope = -GetSlope(x - 0.5, y + 0.5, position.X, position.Y, true);
                            }

                            VisiblePoints.Add(new Point(x, y));
                        }
                        y--;
                    }
                    y++;
                    break;

                case 8: //wnw

                    x = position.X - pDepth;
                    if (x < 0) return;

                    y = position.Y - Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
                    if (y < 0) y = 0;

                    while (GetSlope(x, y, position.X, position.Y, true) >= pEndSlope)
                    {

                        if (GetVisDistance(x, y, position.X, position.Y) <= visrange2)
                        {

                            if (Map[x, y].BlockSight)
                            {
                                if (y - 1 >= 0 && !Map[x, y - 1].BlockSight)
                                    ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x + 0.5, y - 0.5, position.X, position.Y, true), position, visualRange);

                            }
                            else
                            {
                                if (y - 1 >= 0 && Map[x, y - 1].BlockSight)
                                    pStartSlope = GetSlope(x - 0.5, y - 0.5, position.X, position.Y, true);
                            }

                            VisiblePoints.Add(new Point(x, y));
                        }
                        y++;
                    }
                    y--;
                    break;
            }


            if (x < 0)
                x = 0;
            else if (x >= Map.GetLength(0))
                x = Map.GetLength(0) - 1;

            if (y < 0)
                y = 0;
            else if (y >= Map.GetLength(1))
                y = Map.GetLength(1) - 1;

            if (pDepth < visualRange & !Map[x, y].BlockSight)
                ScanOctant(pDepth + 1, pOctant, pStartSlope, pEndSlope, position, visualRange);

        }
        
        private double GetSlope(double pX1, double pY1, double pX2, double pY2, bool pInvert)
        {
            if (pInvert)
                return (pY1 - pY2) / (pX1 - pX2);
            return (pX1 - pX2) / (pY1 - pY2);
        }
        
        private int GetVisDistance(int pX1, int pY1, int pX2, int pY2)
        {
            return ((pX1 - pX2) * (pX1 - pX2)) + ((pY1 - pY2) * (pY1 - pY2));
        }
        
        
    }
}