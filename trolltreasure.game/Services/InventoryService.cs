﻿using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.game.Services
{
    public class InventoryService
    {
        public Result PickUp(GameObject obj, Dungeon dungeon)
        {
            GameObject item = null;
            Result result = new Result();
            foreach (var o in dungeon.Objects)
            {
                if (o.Position.Equals(obj.Position) && o.Item != null)
                {
                    item = o;
                }
            }

            if (item == null)
            {
                result.Logs.Add(new Log($"There is nothing to pick in this position!", MessageType.ActionFailed));
                return result;
            }
            
            if (obj.Items.Count >= 10)
            {
                result.Logs.Add(new Log($"Your inventory is full, cannot pick up {item.Name}", MessageType.ActionFailed));
                return result;
            }
            
            obj.Items.Add(item.Item);
            dungeon.Objects.Remove(item); // so it is not on the floor anymore
            result.Logs.Add(new Log($"You picked up a {item.Name}!", MessageType.ActionSuccess));
            if (item.Item.Equipment != null && item.Item.Equipment.ItemEquippedInSlot(obj, item.Item.Equipment.Slot) == null)
            {
                var equip = item.Item.Equipment.Equip(obj, item.Item);
                result.Logs.AddRange(equip.Logs);
            }
            return result;
        }

        public Result DropItem(GameObject obj, Item item, Dungeon dungeon)
        {
            var result = new Result();
            var position = new Position(obj.Position.X, obj.Position.Y);
            var droppedItem = new GameObject(position, ObjectType.Item, item.Name) {Item = item};
            dungeon.Objects.Add(droppedItem);
            if (item.Equipment != null && item.Equipment.IsEquipped)
            {
                var dequip = item.Equipment.Dequip(obj, item);
                result.Logs.AddRange(dequip.Logs);
            }
            obj.Items.Remove(item);
            result.Logs.Add(new Log($"Dropped the item {droppedItem.Name}", MessageType.ActionSuccess));
            return result;
        }

        public InventoryResult UseItem(GameObject obj, Item item, Dungeon dungeon, Position target)
        {
            if (obj == null)
            {
                return new InventoryResult { Logs = { new Log("object not found", MessageType.Inventory)}};
            }
            if (!obj.Items.Contains(item))
            {
                return new InventoryResult { Logs = { new Log("Item not found", MessageType.Inventory)}};
            }
            
            
            var useItemResult = item.Use?.UseBy(obj, target, dungeon);  //target could be null
            var equipItemResult = item.Equipment?.ToggleEquip(obj, item); 
                    
            if (useItemResult != null && useItemResult.Used)
            {
                obj.Items.Remove(item);
                useItemResult.Logs.Add(new Log($"Item {item.Name} has been used by {obj.Name}", MessageType.Inventory));
                useItemResult.Logs.Add(new Log($"Item {item.Name} has been removed from Item list of {obj.Name}", MessageType.Inventory));
                return useItemResult;
            }
            
            return equipItemResult ?? new InventoryResult { Logs = { new Log($"Item {item.Name} couldn't be used", MessageType.Inventory)}};
        }
        
        
        
    }
}