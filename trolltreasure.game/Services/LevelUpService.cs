﻿using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.game.Services
{
    public class LevelUpService
    {
        // experience and level-ups
        private const int LevelUpBase = 200;
        private const int LevelUpFactor = 150;
        
        public bool LevelUp(Fighter fighter)
        {
            return fighter.Xp >= ToLevelUp(fighter);
        }

        public static int ToLevelUp(Fighter fighter)
        {
            return LevelUpBase + fighter.Level * LevelUpFactor;
        }

        public Result LevelUpConstitution(Fighter fighter)
        {
            var r = new Result();
            fighter.Level += 1;
            fighter.Xp -= ToLevelUp(fighter);
            fighter.MaxHP += 20;
            fighter.HP += 20;
            r.Logs.Add(new Log($"Your battle skills grow stronger! You reached level {fighter.Level} and feel more stamina", MessageType.Information));
            return r;
        }
        
        public Result LevelUpStrength(Fighter fighter)
        {
            var r = new Result();
            fighter.Level += 1;
            fighter.Xp -= ToLevelUp(fighter);
            fighter.Power += 1;
            r.Logs.Add(new Log($"Your battle skills grow stronger! You reached level {fighter.Level} and feel more poweful", MessageType.Information));
            return r;
        }
        
        public Result LevelUpAgility(Fighter fighter)
        {
            var r = new Result();
            fighter.Level += 1;
            fighter.Xp -= ToLevelUp(fighter);
            fighter.Defense += 1;
            r.Logs.Add(new Log($"Your battle skills grow stronger! You reached level {fighter.Level} and feel more agile", MessageType.Information));
            return r;
        }
    }
}