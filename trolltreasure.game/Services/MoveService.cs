﻿using System;
using System.Collections.Generic;
using System.Linq;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;
using trolltreasure.game.Interfaces;

namespace trolltreasure.game.Services
{
    public class MoveService : IMoveService, IPathFinding
    {
        private readonly AttackService _attackService = new AttackService();
        
        public void MoveObject(GameObject obj, int dx, int dy, Tile[,] map, List<GameObject> objects)
        {
            var x = obj.Position.X + dx;
            var y = obj.Position.Y + dy;
            var newPosition = new Position(x, y);
            
            if ( x <= map.GetLength(0) - 1
                && x >= 0 
                && y <= map.GetLength(1) - 1 
                && y >= 0 
                && !map[x, y].Blocked  
                && !objects.Any(o => o.Blocks && o.Position.Equals(newPosition)))
            {
                obj.Position.X += dx;
                obj.Position.Y += dy; 
            }
        }

        public Result MoveOrAttack(GameObject obj, int dx, int dy, Tile[,] map, List<GameObject> objects)
        {
            var x = obj.Position.X + dx;
            var y = obj.Position.Y + dy;
            var pos = new Position(x, y);
            var result = new Result();

            GameObject target = null;
            foreach (var o in objects)
            {
                if (o.Fighter != null && o.Fighter.Status !=FighterStatus.Dead && o.Position.Equals(pos))
                {
                    target = o;
                }
            }

            if (target != null)
            {
                var damage = _attackService.CalculateDamage(obj, target);
                result.Logs.AddRange(damage.Logs);
                result.Logs.Add(new Log($"{obj.Name} infict {damage.Damage} HP of damage to {target.Name}", MessageType.ActionSuccess));

                if (target.Fighter.CheckStatus() == FighterStatus.Dead)
                {
                    obj.Fighter.Xp += target.Fighter.Xp;  // getting more experience
                    result.Logs.Add(new Log($"{target.Name} has been killed! You gain {target.Fighter.Xp} experience points", MessageType.ActionSuccess));
                }
                else
                {
                    result.Logs.Add(new Log($"{target.Name} has {target.Fighter.HP} HP point remaining", MessageType.Information));    
                } 
                
            }
            else
            {
                var prevPosX = obj.Position.X;
                var prevPosY = obj.Position.Y;
                MoveObject(obj, dx, dy, map, objects);
                result.Logs.Add(new Log($"{obj.Name} move from ({prevPosX},{prevPosY}) to position ({obj.Position.X},{obj.Position.Y})", MessageType.Movement));
            }

            return result;
        }
        
        public List<Position> Path(Position from, Position to, Tile[,] map, List<Position> objects)
        {
            throw new NotImplementedException();
        }

        public Position NextMove(Position from, Position to, Tile[,] map, List<Position> objectPostiPositions)
        {
            //vector from this object to the target, and distance
            var diffPosition = to.Diff(from);
            var distance = from.Distance(to);

            //normalize it to length 1 (preserving direction), then round it and
            //convert to integer so the movement is restricted to the map grid
            var dx = diffPosition.X / distance;
            var dy = diffPosition.Y / distance;
            return new Position(dx, dy);
            
        }
    }
}