﻿using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.game.Services
{
    public class TurnBasedService
    {
        public TurnResult TakeTurn(Dungeon dungeon)
        {
            var result = new TurnResult();
            // sort _objects by HP so dead are drawn first    
            dungeon.Objects.Sort();
            // Print objects in player FOV
            foreach (var o in dungeon.Objects)
            {
                // check status of object
                o.UpdateStatus();
                // take turn if they are intelligent enough
                if (o.Ai != null)
                {
                    var logs = o.Ai.TakeTurn(o, dungeon.Map.Tiles, dungeon.Objects);
                    result.Logs.AddRange(logs.Logs);
                }
                   
                // add visible objects
                if (dungeon.Player.Fov.IsPositionVisible(o.Position.X, o.Position.Y))
                {
                    result.ObjectsToDraw.Add(o);
                }
            }
            
            return result;
        }
    }
}