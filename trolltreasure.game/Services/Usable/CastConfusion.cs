﻿using System.Drawing;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;
using trolltreasure.game.Interfaces;
using trolltreasure.game.Services.Behaviors;

namespace trolltreasure.game.Services.Usable
{
    public class CastConfusion : IUsable
    {
        private const int Range = 8;
        public bool Targetable => true;
        
        public InventoryResult UseBy(GameObject caster, Position target,  Dungeon dungeon)
        {
            var result = new InventoryResult();
            if (target == null || target.Equals(caster.Position))
            {
                result.Logs.Add(new Log("No target selected", MessageType.ActionFailed));
                return result;
            }
            
            var targetObject = dungeon.Objects.Find(o => o.Position.Equals(target) && o.Fighter != null && o.Position.Distance(caster.Position) <= Range);
            if (targetObject == null)
            {
                result.Logs.Add(new Log($"There is no reachable enemy to confuse", MessageType.Cast));
            }
            else
            {
                var oldBehavoir = targetObject.Ai;
                targetObject.Ai = new Confused(oldBehavoir);
                result.Used = true;
                result.Logs.Add(new Log($"The eyes of the {targetObject.Name} look vacant, as he starts to stumble around!", MessageType.Cast));
            }
            return result;
        }


    }
}