﻿using System.Drawing;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;
using trolltreasure.game.Interfaces;
using trolltreasure.game.Utils;

namespace trolltreasure.game.Services.Usable
{
    public class CastFireball : IUsable
    {
        public bool Targetable => true;
        private const int Damage = 12;
        private const int Radius = 3;
        
        public InventoryResult UseBy(GameObject caster, Position target, Dungeon dungeon)
        {
            var result = new InventoryResult();
            if (target == null || target.Equals(caster.Position))
            {
                result.Logs.Add(new Log("No target selected", MessageType.ActionFailed));
                return result;
            }
            
            var radiusObject = dungeon.Objects.FindAll(o => o.Position.Distance(target) <= Radius && o.Fighter != null);
            if (radiusObject.Count == 0) result.Logs.Add(new Log($"A lot of fire but didn't reach anyone", MessageType.OnFire));;
            foreach (var o in radiusObject)
            {
                result.Logs.Add(new Log($"The {o.Name} gets burned for {Damage} hit points.", MessageType.OnFire));;
                o.Fighter.TakeDamage(Damage);
            }
            result.Used = true;
            return result;
        }
    }
}