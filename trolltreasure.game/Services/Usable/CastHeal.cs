﻿using System.Drawing;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;
using trolltreasure.game.Interfaces;

namespace trolltreasure.game.Services.Usable
{
    public class CastHeal : IUsable
    {
        private const int HealthAmount = 4;

        public bool Targetable => false;

        public InventoryResult UseBy(GameObject caster, Position target, Dungeon dungeon)
        {
            var usable = new InventoryResult();
            
            if (caster.Fighter != null)
            {
                if (caster.Fighter.HP < caster.Fighter.MaxHP)
                {
                    var prev = caster.Fighter.HP;
                    caster.Fighter.Heal(HealthAmount);
                    usable.Logs.Add(new Log($"{caster.Name} has increase his HP from {prev} to {caster.Fighter.HP}", MessageType.Cast));
                    usable.Used = true;
                }
                else
                {
                    usable.Logs.Add(new Log($"{caster.Name} has already maximum Health ({caster.Fighter.HP})", MessageType.Cast));
                }

                return usable;
            }
            usable.Logs.Add(new Log("Cannot heal this object", MessageType.Cast));
            return usable;
        }
    };
}