﻿using System.Collections.Generic;
using System.Drawing;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;

namespace trolltreasure.game.Services.Usable
{
    public class CastLightning : CastToCloserEnemy
    {
        private const int Damage = 20;
        protected override int Range => 5;
        public override bool Targetable => false;

        public override InventoryResult UseBy(GameObject caster, Position target,  Dungeon dungeon)
        {
            var result = new InventoryResult();
            var closestMonster = ClosestTarget(caster, dungeon.Objects);
            if (closestMonster == null)
            {
                result.Logs.Add(new Log($"No enemy is close enough to strike", MessageType.Cast));
            }
            else
            {
                closestMonster.Fighter.TakeDamage(Damage);
                result.Used = true;
                result.Logs.Add(new Log($"A lighting bolt strikes the {closestMonster.Name} with a loud thunder! The damage is {Damage} hit points.", MessageType.Cast));
            }
            return result;
        }
    }
}