﻿using System.Collections.Generic;
using System.Drawing;
using trolltreasure.game.Domain;
using trolltreasure.game.Domain.Enums;
using trolltreasure.game.Domain.Results;
using trolltreasure.game.Interfaces;
using trolltreasure.game.Utils;

namespace trolltreasure.game.Services.Usable
{
    public abstract class  CastToCloserEnemy : IUsable
    {
        protected virtual int Range => 1;
        public abstract bool Targetable { get; }
        public abstract InventoryResult UseBy(GameObject caster, Position target, Dungeon dungeon);

        protected GameObject ClosestTarget(GameObject obj, List<GameObject> others)
        { 
            // get only fighters that are not the invoker and it is in his FOV and are not dead
            var monsters = others.FindAll(o => o.Fighter != null && o.Fighter.Status != FighterStatus.Dead && o != obj && obj.Fov.IsPositionVisible(o.Position.X, o.Position.Y)); 
            GameObject monster = null;
            var distance = Range; 
            foreach (var m in monsters)
            {
                var dis = obj.Position.Distance(m.Position);
                if (dis < distance)
                {
                    monster = m;
                    distance = (int) dis;
                }
            }
            return monster;
        }

        
    }
}