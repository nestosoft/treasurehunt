﻿using System;
using trolltreasure.game.Domain;

namespace trolltreasure.game.Utils
{
    public static class MapGeneration
    {
        public static void AddWall(Tile[,] map, int x, int y)
        {
            map[x, y].Blocked = true;
            map[x, y].BlockSight = true;
        }

        public static void CreateRoom(Tile[,] map, Room room)
        {
            for (var i = room.X1 + 1 ; i < room.X2; i++)
            {
                for (var j = room.Y1 + 1 ; j < room.Y2; j++)
                {
                    map[i, j].Blocked = false;
                    map[i, j].BlockSight = false;
                }
            }
        }
        
        public static void CreateHorizontalTunnel(Tile[,] map, int x1, int x2, int y)
        {
            for (var x = Math.Min(x1, x2); x < Math.Max(x1, x2); x++)
            {
                map[x, y].Blocked = false;
                map[x, y].BlockSight = false;
            }   
        }
        
        public static void CreateVerticalTunnel(Tile[,] map, int y1, int y2, int x)
        {
            for (var y = Math.Min(y1, y2); y < Math.Max(y1, y2); y++)
            {
                map[x, y].Blocked = false;
                map[x, y].BlockSight = false;
            }   
        }
        
    }
}