﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace trolltreasure.game.Utils
{
    public static class RandomGeneration
    {
        private static readonly Random Random = new Random(1); // seed 1 for now
        
        public static int GetNextRandomBetween(int min, int max)
        {
            return Random.Next(min, max);

        }

        public static string RandomChoice(Dictionary<string, int> chances)
        {
            var sumChoices = chances.Values.Sum();
            var dice = Random.Next(1, sumChoices);

            var runningChance = 0;
            var choice = 0;
            foreach (var chance in chances)
            {
                runningChance += chance.Value;
                if (dice <= runningChance) return chances.Keys.ElementAt(choice);
                choice += 1;
            }
            // should never reach this... but just in case
            return chances.Keys.ElementAt(choice);
        } 
       
    }
}